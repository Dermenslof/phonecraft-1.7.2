package fr.ironcraft.phonecraft.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

/**
 * @authors Dermenslof, DrenBx
 */
public class ModelRelayTelecomWall extends ModelBase
{
	ModelRenderer supportH;
	ModelRenderer supportB;
	ModelRenderer antenna;

	public ModelRelayTelecomWall()
	{
		textureWidth = 64;
		textureHeight = 32;

		supportH = new ModelRenderer(this, 16, 0);
		supportH.addBox(-1F, -2F, 7F, 2, 1, 6);
		supportH.setRotationPoint(0F, 24F, 0F);
		supportH.setTextureSize(64, 32);
		supportH.mirror = true;
		setRotation(supportH, 0.7679449F, 0F, 0F);
		supportB = new ModelRenderer(this, 16, 0);
		supportB.addBox(-1F, -8F, -1F, 2, 1, 6);
		supportB.setRotationPoint(0F, 24F, 0F);
		supportB.setTextureSize(64, 32);
		supportB.mirror = true;
		setRotation(supportB, -0.7679449F, 0F, 0F);
		antenna = new ModelRenderer(this, 0, 0);
		antenna.addBox(-3F, -16F, 3F, 6, 16, 2);
		antenna.setRotationPoint(0F, 24F, 0F);
		antenna.setTextureSize(64, 32);
		antenna.mirror = true;
		setRotation(antenna, 0F, 0F, 0F);
	}

	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		supportH.render(f5);
		supportB.render(f5);
		antenna.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e)
	{
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
	}
}
