package fr.ironcraft.phonecraft.common.items;

import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import fr.ironcraft.phonecraft.client.render.RenderTileEntityAntena;
import fr.ironcraft.phonecraft.client.render.RenderTileEntityRelayTelecom;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityAntena;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityRelayTelecom;
import fr.ironcraft.phonecraft.model.ModelParabole;
import fr.ironcraft.phonecraft.model.ModelPied;

/**
 * @authors Dermenslof, DrenBx
 */
public class RenderItemAntena implements IItemRenderer
{

	private ModelParabole model;
	private ModelPied model1;

	public RenderItemAntena()
	{
		model = new ModelParabole();
	}

	public boolean handleRenderType(ItemStack itemstack, ItemRenderType type)
	{
		return true;
	}

	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack itemstack, ItemRendererHelper helper)
	{
		return true;
	}

	public void renderItem(ItemRenderType type, ItemStack item, Object... data)
	{
		RenderTileEntityAntena.instance.renderTileEntityAtItem(new TileEntityAntena(), 0.0D, 0.0D, 0.0D, 0.0F);
	}
}
