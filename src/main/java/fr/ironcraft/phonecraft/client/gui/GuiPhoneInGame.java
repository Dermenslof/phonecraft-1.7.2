package fr.ironcraft.phonecraft.client.gui;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.config.Configuration;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.client.ClientProxy;
import fr.ironcraft.phonecraft.client.KeyHandler;
import fr.ironcraft.phonecraft.client.gui.button.EnumPhoneButton;
import fr.ironcraft.phonecraft.client.gui.button.GuiPhoneButton;
import fr.ironcraft.phonecraft.client.gui.button.GuiPhoneButtonHome;
import fr.ironcraft.phonecraft.client.gui.button.GuiPhoneButtonLink;
import fr.ironcraft.phonecraft.client.gui.notification.EnumPriority;
import fr.ironcraft.phonecraft.client.gui.notification.EnumType;
import fr.ironcraft.phonecraft.client.gui.notification.Notification;
import fr.ironcraft.phonecraft.network.ThreadNetWork;
import fr.ironcraft.phonecraft.utils.TextureUtils;

/**
 * @authors Dermenslof, DrenBx
 */
@SideOnly(Side.CLIENT)
public class GuiPhoneInGame extends GuiControlPlayer
{
	protected static String wallpaperName = "default";

	protected boolean changeGui = true;
	protected boolean isOpen = true;
	protected boolean isAnimated = false;
	protected boolean isHome = false;
	protected boolean isFullscreen = false;
	protected boolean isCamera;
	protected boolean shootCamera;

	public boolean hideGui;
	public boolean hidePhone;
	protected boolean animPhoto;
	protected float angle;
	protected int changePoint;
	protected float scale = 1;

	protected static ResourceLocation texturePhone;
	private static ResourceLocation textureIcons;
	private static ResourceLocation textureIcons_white;

	private int screenWidth;
	private int screenHeight;

	/* For mouse gestion */
	protected int screen;
	protected EnumPhoneButton buttonType = EnumPhoneButton.NULL;
	protected int bouton;
	protected int app;
	protected static int page;

	protected static int wallpaper = -1;

	public static boolean isActive = true;
	public static int netWork = -1;
	public static boolean has3g;
	public static int lastNetWork = 0;
	public static long last = 0;

	protected boolean needNetwork;
	private static Thread networkGetter = null;
	
	protected static TimeZone timeZone = new GregorianCalendar().getTimeZone();

	public int taskbarshift;
	public boolean taskbarSelected;
	public boolean taskbarEnable;
	private int taskbarScroll = 0;
	protected boolean doVibrate;
	
	private static List<Notification> notifs = new ArrayList<Notification>();
	public static GuiPhoneInGame instance;
	
	private int idShow = -1;
	
	public GuiPhoneInGame(Minecraft par1Minecraft)
	{
		this(par1Minecraft, "phone", "icons/default");
		instance = this;
		if (!Phonecraft.firstOpen && this instanceof GuiPhoneMenu && mc.thePlayer != null)
		{
			Phonecraft.firstOpen = false;
			Phonecraft.config.get(Configuration.CATEGORY_GENERAL, "firstOpen", true).set(false);
			Phonecraft.config.save();
			addNotification(new Notification(EnumPriority.NORMAL, "First open, check this123456", EnumType.LINK, "https://bitbucket.org/Dermenslof/phonecraft-1.7.2/overview"));
		}
	}

	public GuiPhoneInGame(Minecraft par1Minecraft,  String phoneType)
	{
		this(par1Minecraft, phoneType, "icons/default");
	}

	public GuiPhoneInGame(Minecraft par1Minecraft, String phoneType, String iconsType)
	{
		super(par1Minecraft);
		setTexturePhone(phoneType);
		setTextureIcons(iconsType);
		setTextureIcons_white("icons/default_white");
		isActive = true;
		if (this.mc.thePlayer == null)
			return;
		if (networkGetter == null)
		{
			networkGetter = new Thread(new ThreadNetWork(this));
			networkGetter.start();
		}
	}

	@Override
	public void initGui()
	{
		super.initGui();
		
		//buttons
		GuiPhoneButton b = new GuiPhoneButtonHome(this, 0, 36, getScreenHeight() + 4, 23, 20, "");
		b.setIcon(getTextureIcons_white(), 2, 3, 4);
		this.buttonList.add(b);
		b = new GuiPhoneButtonHome(this, 1, getScreenWidth() - 26, getScreenHeight() + 5, 23, 20, "");
		b.setIcon(getTextureIcons_white(), 1, 1, 98);
		this.buttonList.add(b);
		this.taskbarEnable = false;
		this.taskbarSelected = false;
		this.taskbarshift = 0;
		this.mouseMoveY = 0;
		
		this.changeGui = true;
		this.setFont(ClientProxy.fonts.timenewRoman);
	}

	@Override
	public void onGuiClosed()
	{
		isActive = false;
		super.onGuiClosed();
	}

	@Override
	public void updateScreen()
	{
		// test vibreur
		doVibrate = false;
		
		if (!Mouse.isButtonDown(0))
			this.taskbarSelected = false;
		if (!(this instanceof GuiPhoneCamera))
		{
			this.hidePhone = this.screen == 1 ? true : false;
			if (this.mc.thePlayer != null && (this.networkGetter == null || !this.networkGetter.isAlive()))
			{
				this.networkGetter = new Thread(new ThreadNetWork(this));
				this.networkGetter.start();
			}
			if (this.mc.thePlayer != null && this.mc.thePlayer.isDead)
				this.mc.displayGuiScreen(null);
			if (this.isOpen && !this.isAnimated)
			{
				if (this.transparency < 1.0F)
					this.transparency += 0.1F;
			}
			else if (!this.changeGui && !this.isOpen && !this.isAnimated && this.transparency > 0F)
			{
				this.transparency -= 0.1F;
				if (this.transparency < 0F)
					this.transparency = -0F;
			}
			else if (!this.isOpen && !this.isAnimated && this.transparency <= 0F)
				this.mc.displayGuiScreen(new GuiPhoneAnimation(mc, true));
		}
		super.updateScreen();
	}

	public void drawScreen(int par1, int par2, float par3)
	{
		this.mouseX = par1;
		this.mouseY = par2;
		if (!this.hidePhone)
		{
			this.drawShell();
			this.drawForground();
		}
		this.onMouseOverPhone(par1, par2);
		for (Object b: this.buttonList)
				((GuiButton)b).drawButton(mc, par1, par2);
		drawTaskbar(par1, par2);
	}

	private void drawForground()
	{	
		if (!this.isAnimated)
		{
			if (this.wallpaper < 0)
			{
				this.drawGradientRect(0, 0, getScreenWidth(), getScreenHeight(), 0xff1166cc, 0x22000000, -transparency + 2F - this.scale);
				if ((this instanceof GuiPhoneMenu && !this.isHome) || !(this instanceof GuiPhoneMenu))
					this.drawRect(0, 0, getScreenWidth(), getScreenHeight(), 0x000000, -transparency + 2F - this.scale - 0.6F);
			}
			else
			{
				//Wallpaper
				if ((this instanceof GuiPhoneMenu && !this.isHome) || !(this instanceof GuiPhoneMenu))
					drawScreenBackground(0.5F);
				else
					drawScreenBackground(0.8F);
			}
			this.drawRect(0, -10, 92, 0, 0x00000000, 1.0F);
			//time (real)
			GregorianCalendar cal = new GregorianCalendar();
//			for (String t : TimeZone.getAvailableIDs())
//				System.out.println(t);
			timeZone = TimeZone.getDefault();
			cal.setTimeZone(timeZone);
			int hour = cal.get(GregorianCalendar.HOUR);
			String h = cal.get(GregorianCalendar.AM_PM) == 0 ? "" + hour : "" + (hour + 12);
			h = hour < 10 && cal.get(GregorianCalendar.AM_PM) == 0 ? "0" + h : h;
			int minute = cal.get(GregorianCalendar.MINUTE);
			String m =  minute < 10 ? "0" + minute : "" + minute;

			GL11.glPushMatrix();
			GL11.glTranslatef(this.getScreenPosX() + this.getScreenWidth() - 16, this.getScreenPosY() - 10, 0F);
			GL11.glScalef(0.5F, 0.5F, 1F);
			this.drawString(h, 0, 0, 0xd2d2d2, 0.3F);
			this.drawString(":", 12 , -1, 0xd2d2d2, 0.3F);
			this.drawString( m, 16, 0, 0xd2d2d2, 0.3F);
			GL11.glPopMatrix();

			//reseau
			GL11.glPushMatrix();
			GL11.glTranslatef(this.getScreenPosX(), this.getScreenPosY() - 10, 0);
			GL11.glScalef(0.5F, 0.5F, 1);
			if (hasNetwork())
				this.drawString("IC telecom", 0 + 1, 0, 0xd2d2d2, 0.3F);
			else
				this.drawString(I18n.format(I18n.format("gui.phone.nonetwork", new Object[0])), 0 + 1, 0, 0xd2d2d2, 0F);
			GL11.glPopMatrix();

			for (int i=0; i<5; ++i)
				this.drawRect(this.getScreenWidth() - 28 + i * 2, -3, this.getScreenWidth() - 27 + i * 2, -4 - i, 0xff757575, 0);
			if (hasNetwork())
			{
				for (int i=0; i< getNumberOfNetworkBar(); ++i)
					this.drawRect(this.getScreenWidth() - 28 + i * 2, -3, this.getScreenWidth() - 27 + i * 2, -4 - i, 0xff00ff00, 0);
			}
			GL11.glPushMatrix();
			GL11.glScalef(0.55F, 0.55F, 1);
			this.fontRendererObj.drawString("4g", (int)((getScreenPosX() + 56) / 0.55F), (int)((getScreenPosY() -7) / 0.55F), 0xd2d2d2);
			GL11.glPopMatrix();
			if (!has3g)
			{
				GL11.glColor3f(0.75F, 0F, 0F);
				GL11.glDisable(GL11.GL_TEXTURE_2D);
				GL11.glBegin(GL11.GL_LINES);
				GL11.glVertex2d(getScreenPosX() + 55, getScreenPosY() -3);
				GL11.glVertex2d(getScreenPosX() + 62, getScreenPosY() -7);
				GL11.glEnd();
				GL11.glEnable(GL11.GL_TEXTURE_2D);
			}
			
			// notification alert
			if (notifs.size() > 0)
			{
				GL11.glPushMatrix();
				GL11.glColor4f(1F,  1F,  1F,  1F);
				GL11.glTranslatef(this.getScreenPosX(), this.getScreenPosY() - 9, 0);
				GL11.glScalef(0.5F, 0.5F, 1);
				GL11.glEnable(GL11.GL_BLEND);
				this.mc.renderEngine.bindTexture(this.getTextureIcons_white());
				this.drawTexturedModalRect(92, 0, 0 * 16, 5 * 16, 16, 16);
				GL11.glPopMatrix();
			}
		}
		this.drawAbsoluteRect(this.getScreenPosX(), this.getScreenPosY() - 10, this.getScreenPosX() + this.getScreenWidth(), this.getScreenPosY() + this.getScreenHeight(), 0xFF000000, this instanceof GuiPhoneCamera ? 0 : 1F - this.transparency);
	}

	private void drawScreenBackground(float opacity)
	{
		//Wallpaper
		GL11.glPushMatrix();
		GL11.glColor4f(opacity, opacity, opacity, 2F - this.scale);
		GL11.glTranslatef(this.getScreenPosX() + 0.2F + this.shift, this.getScreenPosY(), 0F);
		GL11.glScalef(0.72F / (256 / this.getScreenWidth()), 0.576F / (256 / this.getScreenHeight()), 1);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.wallpaper);
		this.drawTexturedModalRect(0, 0, 0, 0, 255, 256);
		GL11.glPopMatrix();
	}

	private void drawShell()
	{
		//Rotation
		GL11.glTranslatef(this.width / 2, this.height / 2, 0);
		GL11.glRotatef(this.angle, 0, 0, 1);
		GL11.glScalef(this.scale, this.scale, 1);
		
		// test vibreur
		int vibrate = this.doVibrate && System.currentTimeMillis() / 1000 % 2 == 0 ? -5 + new Random().nextInt(5) : 0;
		
		GL11.glTranslatef(-this.width / 2 - this.changePoint / 1.38F  + vibrate, -this.height / 2 - this.changePoint / 15, 0);
		this.drawAbsoluteRect(this.getScreenPosX(), this.getScreenPosY() - 10, this.getScreenPosX() + this.getScreenWidth() + 2, this.getScreenPosY() + this.getScreenHeight(), 0xFF000000, this.animPhoto ? 0F : 1F);
		GL11.glColor4f(1,  1,  1,  1F);
		
		//Texture
		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glTranslatef(this.getScreenPosX() - 4.5F, this.height - 231.5F, 0);
		GL11.glScalef(1.02F, 1.004F, 1);
		this.mc.renderEngine.bindTexture(this.getTexturePhone());
		this.drawTexturedModalRect(0, 0, 0, 0, 110, 225);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();
		
		GL11.glPushMatrix();
		GL11.glColor4f(0.3F,  0.3F,  0.3F,  1F);
		GL11.glEnable(GL11.GL_BLEND);
		this.mc.renderEngine.bindTexture(this.getTextureIcons_white());
		this.drawTexturedModalRect(getScreenPosX() + getScreenWidth() - 25, getScreenPosY() + getScreenHeight() + 6, 2 * 16, 6 * 16, 16, 16);//(80, 38, 162, 1.0F);
		GL11.glPopMatrix();
	}
	
	protected void drawTaskbar(int par1, int par2)
	{
		int y = par2;
		if (this.mouseMoveY != 0 && this.taskbarSelected)
		{
			if (this.mouseMoveY < -10)
				this.taskbarEnable = true;
			else if (this.mouseMoveY > 10)
				this.taskbarEnable = false;
		}
		
		GL11.glTranslatef(0, 0, 1000);
		if (this.taskbarshift > 10)
			this.drawRect(0, 0, getScreenWidth(), this.taskbarshift - 5, 0x101010, 0);
		if (this.taskbarshift > 2)
		{
			this.drawRoundedRect(0, this.taskbarshift < 10 ? 0 : this.taskbarshift - 10, getScreenWidth(), this.taskbarshift, 2, 0x757575, 0);
			this.drawRoundedRect(1, this.taskbarshift < 10 ? 0 : this.taskbarshift - 10 + 1, getScreenWidth() - 1, this.taskbarshift - 1, 2, 0x252525, 0);
		}
		if (this.taskbarshift > 5)
			this.drawHorizontalLine(getScreenPosX() + getScreenWidth() / 2 - 5, getScreenPosX() + getScreenWidth() / 2 + 5, getScreenPosY() + this.taskbarshift - 5, 0xff757575);

		int size = notifs.size();
		int scrollHeight = 0;
		int limit = size  * 8 + 41 > this.taskbarshift ? 125 : 132;
		int limitTitle;
		Notification n = null;
		for (int i=0; i < size; ++i)
		{
			if (20 + i * 8 + this.taskbarScroll >= 20 && 20 + i * 8 + this.taskbarScroll<= this.taskbarshift - 20)
			{
				scrollHeight++;
				n = notifs.get(i);
				limitTitle = n.getType() == EnumType.LINK ? limit - (getFont().getStringWidth("link") + 20) / 2 : limit;
				String title = n.getTitle();
				if (getFont().getStringWidth(title) > limitTitle)
				{
					while (getFont().getStringWidth(title) > limitTitle)
						title = title.substring(0, title.length() - 1);
					title = title.substring(0, title.length() - 4) + " ...";
				}
				Date now = GregorianCalendar.getInstance().getTime();
				Date d = notifs.get(i).getDate();
				long time = new Timestamp(now.getTime()).getTime() - new Timestamp(d.getTime()).getTime();
				String format = now.getDay() != d.getDay() ? "MM/dd" : "HH:mm";
				String date = new SimpleDateFormat(format).format(d);
				this.drawRect(0, 20 + i * 8 + this.taskbarScroll, this.getScreenWidth(), 20 + 9 + i * 8 + this.taskbarScroll, 0xff353535,this.transparency);
				this.drawRect(1, 20 + 1 + i * 8 + this.taskbarScroll, this.getScreenWidth() - 1, 20 + 8 + i * 8 + this.taskbarScroll, 0xff252525,this.transparency);
				if (n.getType() == EnumType.LINK)
				{
					n.getLink().xPosition = getScreenPosX() + limit - 50;//80;
					n.getLink().yPosition = getScreenPosY() + 20 + i * 8 + this.taskbarScroll + 2;
					n.getLink().drawButton(mc, par1, par2);
				}
				GL11.glPushMatrix();
				GL11.glScalef(0.5F, 0.5F, 1);
				this.drawString(date, (int)((this.getScreenPosX() + 2) / 0.5F), (int)((this.getScreenPosY() + 20 + i * 8 + this.taskbarScroll) / 0.5F), 0xffffffff);
				this.drawString(title, (int)((this.getScreenPosX() + 22) / 0.5F), (int)((this.getScreenPosY() + 20 + i * 8 + this.taskbarScroll) / 0.5F), 0xffffffff);
				GL11.glPopMatrix();
			}
		}
		if (size != 1 && this.taskbarshift > 39 && size  * 8 + 41 > this.taskbarshift /*scrollHeight > 0 && scrollHeight < size && scrollHeight * 8 + 41 > this.taskbarshift*/)
		{
			this.drawRect(this.getScreenWidth() - 8, 20, this.getScreenWidth(), 20 + scrollHeight * 8, 0xff353535, 0);
			this.drawRect(this.getScreenWidth() - 7, 20 + 1, this.getScreenWidth() - 1, 19 + scrollHeight * 8 + 1, 0xff252525, 0);
			int maxScroll = size > scrollHeight ? size * 8 - 8 * scrollHeight : 1;
			int percent = ((-this.taskbarScroll * 100) / maxScroll);
			int dec = ((scrollHeight * 8 - 8) * percent) / 100;
			this.drawRect(this.getScreenWidth() - 7, 20 + 1 + dec, this.getScreenWidth() - 1, 20 - 1 + dec + 8, 0xff757575, 0);
		}
		if (n != null && idShow > -1)
		{
			this.drawRect(0, 20 + idShow * 8 + this.taskbarScroll, this.getScreenWidth(), 20 + 9 + idShow * 8 + this.taskbarScroll, 0xff353535,this.transparency);
			this.drawRect(1, 20 + 1 + idShow * 8 + this.taskbarScroll, this.getScreenWidth() - 1, 20 + 8 + idShow * 8 + this.taskbarScroll, 0xff000000,this.transparency);
			GL11.glPushMatrix();
			GL11.glScalef(0.5F, 0.5F, 1);
			this.drawString(n.getTitle(), (int)((this.getScreenPosX() + 2) / 0.5F), (int)((this.getScreenPosY() + 20 + idShow * 8 + this.taskbarScroll) / 0.5F), 0xffffffff);
			GL11.glPopMatrix();
		}
		GL11.glTranslatef(0, 0, -1000);
		
		if (!Mouse.isButtonDown(0))
		{
			if (!this.taskbarEnable)
				this.taskbarshift -= 10;
			else if (this.taskbarEnable)
				this.taskbarshift += 10;
			if (this.taskbarshift > getScreenHeight() - 1)
				this.taskbarshift = getScreenHeight() - 1;
			if (this.taskbarshift < 0)
				this.taskbarshift = 0;
		}
		
	}

	@Override
	protected void keyTyped(char par1, int par2)
	{
		if (par1 == ' ' && !lastJump)
		{
			this.keyDelay = 8;
			this.keySpaceCounter++;
		}
		if (par2 == this.mc.gameSettings.keyBindForward.getKeyCode() && this.isUpRelease && this.mc.thePlayer.onGround)
		{
			this.isUpRelease = false;
			this.keyDelay = 8;
			this.keyUpCounter++;
		}
		if (par2 == 1)
			this.mc.displayGuiScreen(new GuiIngameMenu());
		//		else if (par2 == KeyHandler.key_PhoneFocus.getKeyCode())
		//			this.getFocus() = !this.getFocus();
		else if (par2 == KeyHandler.key_PhoneGUI.getKeyCode())
		{
			this.changeGui = false;
			this.isOpen = !this.isOpen;
		}
		else if (par1 >= '1' && par1 <= '9')
			this.mc.thePlayer.inventory.currentItem = Integer.parseInt("" + par1) - 1;
	}
	
	@Override
	public void handleMouseInput()
	{
		super.handleMouseInput();
		int x = Mouse.getEventX() * this.width / this.mc.displayWidth;
		int y = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;

		//Tactile slide gestion
		if (Mouse.isButtonDown(0) && this.getFocus())
		{	
			if (!(this instanceof GuiPhoneCamera))
			{
				if (!this.taskbarSelected && isInScreen() && y >= this.taskbarshift - 10 + getScreenPosY() && y <= this.taskbarshift + getScreenPosY())
					this.taskbarSelected = true;
				if (this.taskbarSelected && y > getScreenPosY() - 5)
					this.taskbarshift = y - getScreenPosY() + 5;
				if (this.taskbarshift > getScreenHeight() - 1)
					this.taskbarshift = getScreenHeight() - 1;
				if (this.taskbarshift < 0)
					this.taskbarshift = 0;
			}
			if (isInScreen())
			{
				if(!this.mouseIsDrag)
				{
					this.clickX = x;
					this.clickY = y;
					this.mouseIsDrag = true;
				}
				else
				{
					if(this.clickX != x || this.clickY != y)
						this.isTactile = true;
					this.releaseY = y;
					if (this.taskbarSelected && this.mouseMoveY == this.clickY - this.releaseY)
					{
						this.mouseIsDrag = false;
						this.mouseMoveY = 0;
					}
					this.mouseMoveY = this.clickY - this.releaseY;
					this.releaseX = x;
					this.mouseMoveX = this.clickX - this.releaseX;
				}
			}
			else
				this.mouseIsDrag = false;
		}
		else
			this.mouseIsDrag = false;
		
		if (isInScreen() && (this.taskbarEnable || this.taskbarSelected))
		{
			int w = Mouse.getDWheel();
			int size = notifs.size();
			int maxScroll = size > (this.taskbarshift - 32) / 8 ? size * 8 - ((this.taskbarshift - 32) / 8) * 8 : 0;
			if (w != 0 &&  size > 2)
			{
				if (w > 0)
					this.taskbarScroll += 8;
				else if (w < 0)
					this.taskbarScroll -= 8;
				if (this.taskbarScroll < -maxScroll)
					this.taskbarScroll = -maxScroll;
				else if (this.taskbarScroll > 0)
					this.taskbarScroll = 0;
			}
			
			if (this.taskbarEnable)
			{

				int scrollHeight = 0;
				int limit = size  * 8 + 41 > this.taskbarshift ? 125 : 132;
				int limitTitle;
				Notification n = null;
				for (int i=0; i < size; ++i)
				{
					if (20 + i * 8 + this.taskbarScroll >= 20 && 20 + i * 8 + this.taskbarScroll<= this.taskbarshift - 20)
					{
						n = notifs.get(i);
						limitTitle = n.getType() == EnumType.LINK ? limit - (getFont().getStringWidth("link") + 20) / 2 : limit;
						String title = n.getTitle();
						if (x >= getScreenPosX() + 22 && x <= getScreenPosX() + 22 + limitTitle
								&& y >= this.getScreenPosY() + 20 + i * 8 + this.taskbarScroll && y <= this.getScreenPosY() + 20 + i * 8 + this.taskbarScroll + 8)
						{
							this.drawAbsoluteRect(0, 0, 50, 50, 0xff0000, 1F);
						}
					}
				}
			}
		}
	}
	
	@Override
	public void mouseClicked(int x, int y, int k)
	{
		super.mouseClicked(x, y, k);
		if (isInScreen())
		{
			if (this.taskbarEnable)
			{
				int size = notifs.size();
				for (int i=0; i < size; ++i)
				{
					if (y - getScreenPosY() >= 20 + i * 8 + this.taskbarScroll && y - getScreenPosY() <= 28 + i * 8 + this.taskbarScroll)
					{
						if (notifs.get(i).canDestroy() && k == 0)
							notifs.remove(i);
						else if (k == 0)
						{
							GuiPhoneButton b = notifs.get(i).getLink();
							if (b != null)
							{
								GuiPhoneButtonLink bl = (GuiPhoneButtonLink)b;
								bl.action();
							}
						}
						else if (k == 1)
							this.idShow = this.idShow == i ? -1 : i;
						break;
					}
				}
			}
		}
	}
	
	@Override
	protected void actionPerformed(GuiButton button)
	{
		if (button instanceof GuiPhoneButtonLink)
		{
			GuiPhoneButtonLink b = (GuiPhoneButtonLink)button;
			b.action();
			return;
		}
		this.taskbarEnable = false;
		this.mouseMoveY = 0;
		if ((this instanceof GuiPhoneMenu && this.isHome) || this instanceof GuiPhoneCamera)
			return;
		switch (button.id)
		{
		case 0:
			this.isHome = true;
			if (!(this instanceof GuiPhoneMenu))
				this.mc.displayGuiScreen(new GuiPhoneMenu(this.mc, true));
			break;
		case 1:
			getLastGuiPhone();
			break;
		}
	}

	protected void getLastGuiPhone()
	{
		this.buttonType = EnumPhoneButton.NULL;
		this.bouton = -1;
		if (this instanceof GuiPhoneMenu)
		{
			if (!this.isHome)
				this.isHome = true;
		}
		else if (this instanceof GuiPhoneEditImg)
			this.mc.displayGuiScreen(new GuiPhoneImages(this.mc));
		else if (!(this instanceof GuiPhoneMessages) && !(this instanceof GuiPhoneContacts))
		{
			this.isHome = false;
			this.mc.displayGuiScreen(new GuiPhoneMenu(this.mc, false));
		}
		else
			this.mc.displayGuiScreen(new GuiPhoneMenu(this.mc, true));
	}

	protected void onMouseOverPhone(int x, int y)
	{
		this.buttonType = EnumPhoneButton.NULL;
		this.bouton = -1;
		this.app = -1;
	}

	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	protected boolean isInScreen()
	{
		if (getFocus() && Mouse.isInsideWindow())
		{
			int x = Mouse.getEventX() * this.width / this.mc.displayWidth;
			int y = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;
			if (x >= this.getScreenPosX() && x <= this.getScreenPosX() + this.getScreenWidth())
			{
				if (y >= this.getScreenPosY() && y <= this.getScreenPosY() + this.getScreenHeight())
					return true;
			}
		}
		return false;
	}

	/**
	 * get the main Phonecraft texture
	 * 
	 * @author Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 * @return ResourceLocation texture
	 */
	public ResourceLocation getTexturePhone()
	{
		return texturePhone;
	}

	/**
	 * get the main Phonecraft icons
	 * 
	 * @author Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 * @return ResourceLocation texture
	 */
	public ResourceLocation getTextureIcons()
	{
		return textureIcons;
	}

	/**
	 * get the main Phonecraft icons_white
	 * 
	 * @author Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 * @return ResourceLocation texture
	 */
	public ResourceLocation getTextureIcons_white()
	{
		return textureIcons_white;
	}

	/**
	 * set Phonecraft default icon with an position ID (like terrain.png, R.I.P)
	 *"/!\" You must use GL11.glColor4f before use this function
	 * 
	 * 
	 * @author Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 * @param Position of texture on the png
	 * @param Position X on phone screen
	 * @param Position Y on phone screen
	 * @param Define the dimension size
	 * 
	 */
	public void drawIcon(int textureId, int posX, int posY, float textureSize)
	{
		int iconSize = 16;
		int iconPerLine = 16;
		int iconPerCol = 256;
		int iconPosX = (textureId % iconPerLine) * iconSize;
		int iconPosY = ((textureId / iconPerLine) * iconSize) % iconPerCol;

		GL11.glPushMatrix();
		GL11.glColor4f(1,  1,  1,  this.transparency);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glColor4f(1, 1, 1, 1);
		GL11.glScalef(textureSize, textureSize, 1.0F);
		this.mc.renderEngine.bindTexture(this.getTextureIcons());
		GL11.glTranslatef((this.getScreenPosX() + posX) / textureSize, this.getScreenPosY() + posY / textureSize, 0.5F);
		this.drawTexturedModalRect(0, 0, iconPosX, iconPosY, iconSize, iconSize);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();
	}

	/**
	 * set the main texture used by Phonecraft
	 * 
	 * @author Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 * @param filename
	 * @return new phone texture
	 */
	public ResourceLocation setTexturePhone(String phoneType)
	{
		return this.texturePhone = new ResourceLocation(TextureUtils.getTextureNameForGui(phoneType));
	}

	/**
	 * set the main icons used by Phonecraft
	 * 
	 * @author Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 * @param filenam or domain:filename
	 * @return new icons
	 */
	public ResourceLocation setTextureIcons(String iconsType)
	{
		return this.textureIcons = new ResourceLocation(TextureUtils.getTextureNameForGui(iconsType));
	}

	/**
	 * set the main icons used by Phonecraft
	 * 
	 * @author Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 * @param filenam or domain:filename
	 * @return new icons
	 */
	public ResourceLocation setTextureIcons_white(String iconsType)
	{
		return this.textureIcons_white = new ResourceLocation(TextureUtils.getTextureNameForGui(iconsType));
	}

	/**
	 * get if Phonecraft has connected at an network
	 * 
	 * @author Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 * @return true or false
	 */
	public boolean hasNetwork()
	{
		return this.netWork >= 0 && this.netWork <= 50 ? true : false;
	}

	/**
	 * get number of network bar
	 * 
	 * @author Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 * @return number of network bar
	 */
	public int getNumberOfNetworkBar()
	{
		return 5 - this.netWork / 10;
	}
	
	public static void addNotification(Notification n)
	{
		notifs.add(n);
	}
	
	private void deleteNotification(Notification n)
	{
		notifs.remove(n);
	}
}
