package fr.ironcraft.phonecraft.common.tileentities;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.C17PacketCustomPayload;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.packet.PacketRelayTelecom;

/**
 * @authors Dermenslof, DrenBx
 */
public class TileEntityRelayTelecom extends TileEntity
{
	public boolean isConnected;
	
    public Packet getDescriptionPacket()
	{
        return null;
	}
	
	@Override
    public void onDataPacket (NetworkManager net, S35PacketUpdateTileEntity packet)
    {
        readFromNBT(packet.func_148857_g());
        markDirty();
        worldObj.func_147479_m(xCoord, yCoord, zCoord);
    }
	
	@Override
    public void readFromNBT(NBTTagCompound par1NBTTagCompound)
    {
    	super.readFromNBT(par1NBTTagCompound);
    	this.isConnected = par1NBTTagCompound.getBoolean("online");
    }

    @Override
    public void writeToNBT(NBTTagCompound par1NBTTagCompound)
    {
       	super.writeToNBT(par1NBTTagCompound);
    	par1NBTTagCompound.setBoolean("online", this.isConnected);
    }
}
