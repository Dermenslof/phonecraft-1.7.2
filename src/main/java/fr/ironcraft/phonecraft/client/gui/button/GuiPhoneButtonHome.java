package fr.ironcraft.phonecraft.client.gui.button;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;

import org.lwjgl.opengl.GL11;

import fr.ironcraft.phonecraft.client.gui.GuiPhoneInGame;

public class GuiPhoneButtonHome extends GuiPhoneButton
{
	public GuiPhoneButtonHome(GuiPhoneInGame gui, int id, int x, int y, String title)
    {
        super(gui, id, x, y, 23, 23, title);
    }
	
	public GuiPhoneButtonHome(GuiPhoneInGame gui, int id, int x, int y, int width, int height, String title)
	{
		super(gui, id, x, y, width, height, title);
	}
	
    public void drawButton(Minecraft p_146112_1_, int p_146112_2_, int p_146112_3_)
    {
        if (this.visible)
        {
            FontRenderer fontrenderer = p_146112_1_.fontRenderer;
            GL11.glColor4f(1.0F, 1.0F, 1.0F, gui.getTransparency());
            this.field_146123_n = p_146112_2_ >= this.xPosition && p_146112_3_ >= this.yPosition && p_146112_2_ < this.xPosition + this.width && p_146112_3_ < this.yPosition + this.height;
            int k = this.getHoverState(this.field_146123_n);
            if (this.field_146123_n && hasIcon() && gui.getFocus())
            {
            	GL11.glPushMatrix();
            	GL11.glEnable(GL11.GL_BLEND);
            	GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            	if (customTexture)
            		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture);
            	else
            		gui.mc.renderEngine.bindTexture(img);
            	GL11.glTranslatef(this.xPosition + iconX + (this.id == 0 ? 0.5F : 0), this.yPosition + iconY, 0);
            	GL11.glScalef(textureScale, textureScale, 1F);
            	this.drawTexturedModalRect(0, 0, textureOffsetX, textureOffsetY, textureWidth, textureHeight);
            	GL11.glDisable(GL11.GL_BLEND);
            	GL11.glPopMatrix();
            }
            this.mouseDragged(p_146112_1_, p_146112_2_, p_146112_3_);
            int l = 14737632;

            if (packedFGColour != 0)
                l = packedFGColour;
            else if (!this.enabled)
                l = 10526880;
            else if (this.field_146123_n)
                l = 16777120;
            this.drawCenteredString(fontrenderer, this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, l);
        }
    }
}
