package fr.ironcraft.phonecraft.client;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;
import paulscode.sound.SoundSystemConfig;
import paulscode.sound.SoundSystemException;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import de.cuina.fireandfuel.CodecJLayerMP3;
import fr.ironcraft.phonecraft.client.render.RenderTileEntityAntena;
import fr.ironcraft.phonecraft.client.render.RenderTileEntityQrCode;
import fr.ironcraft.phonecraft.client.render.RenderTileEntityRelayTelecom;
import fr.ironcraft.phonecraft.common.CommonProxy;
import fr.ironcraft.phonecraft.common.blocks.ICBlocks;
import fr.ironcraft.phonecraft.common.items.RenderItemAntena;
import fr.ironcraft.phonecraft.common.items.RenderItemRelayTelecom;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityAntena;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityQrCode;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityRelayTelecom;
import fr.ironcraft.phonecraft.utils.ImageLoader;

/**
 * @author Dermenslof, DrenBx
 */
public class ClientProxy extends CommonProxy
{
	private static Minecraft mc = Minecraft.getMinecraft();
	public static PhoneAchievements achievements;
	public static ImageLoader imageLoader = new ImageLoader();
	public static CustomFonts fonts = new CustomFonts();
	public static int renderQrCodeID, renderRelayTelecomID, renderAntenaID;
	public AppRegistry appRegistry;
	
	@Override
	public void init()
	{
		try
		{
			SoundSystemConfig.setCodec("mp3", CodecJLayerMP3.class);
		}
		catch (SoundSystemException e)
		{
			System.err.println("error linking with the LibraryJavaSound plug-in");
			e.printStackTrace();
		}
		appRegistry = new AppRegistry(mc);
		achievements = new PhoneAchievements();
		achievements.init();
		fonts.init();
		events();
		renders();
	}
	
	public void renders()
	{
		renderQrCodeID = RenderingRegistry.getNextAvailableRenderId();
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityQrCode.class, new RenderTileEntityQrCode());
		renderRelayTelecomID = RenderingRegistry.getNextAvailableRenderId();
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ICBlocks.relayTelecom), new RenderItemRelayTelecom());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityRelayTelecom.class, new RenderTileEntityRelayTelecom());
		renderAntenaID = RenderingRegistry.getNextAvailableRenderId();
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ICBlocks.antena), new RenderItemAntena());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityAntena.class, new RenderTileEntityAntena());
	}
	
	public void events()
	{
		MinecraftForge.EVENT_BUS.register(new EventHandler());
		FMLCommonHandler.instance().bus().register(new KeyHandler());
	}
}
