package fr.ironcraft.phonecraft.client.gui;

import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import fr.ironcraft.phonecraft.client.KeyHandler;

/**
 * @authors Dermenslof, DrenBx
 */
public class GuiControlPlayer extends GuiScreenCustom
{
	public Minecraft mc;

	private boolean isFocused;
	private boolean hasMoveControl;
	private boolean hasClickControl;

	protected boolean isLeftClickUp;
	protected int clickX;
	protected int clickY;
	protected int releaseX;
	protected int releaseY;
	protected int mouseX;
	protected int mouseY;
	protected int mouseMoveX;
	protected int mouseMoveY;
	protected static int lastMouseX = -1;
	protected static int lastMouseY = -1;
	
	protected boolean isTactile;
	protected boolean mouseIsDrag;
	
	protected boolean isHittingBlock;
	protected int currentBlockX;
	protected int currentBlockY;
	protected int currentblockZ;
	protected ItemStack heldItem;
	protected float curBlockDamageMP;
	protected int rightClickTimer = 4;
	protected float stepSoundTickCounter;
	protected int keySpaceCounter = 0;
	protected int keyUpCounter = 0;
	protected int keyDelay = 10;
	protected int keySpaceDelay = 10;
	protected boolean lastJump;
	protected boolean isRunning;
	protected boolean isUpRelease;

	public GuiControlPlayer(Minecraft mc)
	{
		this.mc = mc;
		this.hasClickControl = true;
		this.hasMoveControl =true;
	}
	
	@Override
	public void initGui()
	{
		Keyboard.enableRepeatEvents(true);
		this.setFocus(false, true);
		if (lastMouseX > -1 && Keyboard.isKeyDown(KeyHandler.key_PhoneFocus.getKeyCode()) && Mouse.isInsideWindow())
		{
			applyFocus();
			Mouse.setCursorPosition(lastMouseX, lastMouseY);
		}
	}
	
	@Override
	public void onGuiClosed()
	{
		lastMouseX = Mouse.getX();
		lastMouseY = Mouse.getY();
		super.onGuiClosed();
	}
	
	@Override
	public void updateScreen()
	{
		applyFocus();
		if (!Mouse.isButtonDown(0))
			this.mc.playerController.resetBlockRemoving();
		if (!Keyboard.isKeyDown(mc.gameSettings.keyBindForward.getKeyCode()))
		{
			isUpRelease = true;
			isRunning = false;
		}
		if (rightClickTimer > 0)
			--this.rightClickTimer;
		if (keySpaceDelay > 0)
			--keySpaceDelay;
		if (keyDelay > 0)
			--keyDelay;
		else
		{
			keySpaceCounter = 0;
			keyUpCounter = 0;
		}
		this.KeyMovement();
		if (isFocused)
			return;
		int mouseButton = Mouse.isButtonDown(0) ? 0 : (Mouse.isButtonDown(1) ? 1 : -1);
		if (hasClickControl)
			this.clickMouse(mouseButton);
	}
	
	/*  player focused into this GUI */
	private void KeyMovement()
	{
		if (!hasMoveControl)
			return;
		boolean up = Keyboard.isKeyDown(mc.gameSettings.keyBindForward.getKeyCode());
		boolean down = Keyboard.isKeyDown(mc.gameSettings.keyBindBack.getKeyCode());
		boolean jump = Keyboard.isKeyDown(mc.gameSettings.keyBindJump.getKeyCode());
		boolean left = Keyboard.isKeyDown(mc.gameSettings.keyBindLeft.getKeyCode());
		boolean right = Keyboard.isKeyDown(mc.gameSettings.keyBindRight.getKeyCode());
		boolean sneak = Keyboard.isKeyDown(mc.gameSettings.keyBindSneak.getKeyCode());
		KeyBinding.setKeyBindState(mc.gameSettings.keyBindForward.getKeyCode(), up);
		KeyBinding.setKeyBindState(mc.gameSettings.keyBindBack.getKeyCode(), down);
		KeyBinding.setKeyBindState(mc.gameSettings.keyBindJump.getKeyCode(), jump);
		KeyBinding.setKeyBindState(mc.gameSettings.keyBindLeft.getKeyCode(), left);
		KeyBinding.setKeyBindState(mc.gameSettings.keyBindRight.getKeyCode(), right);
		KeyBinding.setKeyBindState(mc.gameSettings.keyBindSneak.getKeyCode(), sneak);
	}
	
	protected void applyFocus()
	{
		if(Keyboard.isKeyDown(KeyHandler.key_PhoneFocus.getKeyCode()))
		{
			if(!this.getFocus())
				this.setFocus(true, true);
		}
		else if (!Keyboard.isKeyDown(KeyHandler.key_PhoneFocus.getKeyCode()) && this.getFocus())
			this.setFocus(false, true);
	}

	@Override
	protected void mouseMovedOrUp(int x, int y, int which)
    {
		super.mouseMovedOrUp(x, y, which);
		if (which == 0)
		{
			isLeftClickUp = true;
			mc.playerController.resetBlockRemoving();
		}
	}
	
	@Override
	public void mouseClicked(int i, int j, int k)
	{
		super.mouseClicked(i, j, k);
	}
	
	@Override
	public void handleMouseInput()
	{
		super.handleMouseInput();
		if (!hasClickControl)
			return;
		this.wheelMouse();
	}
	
	private void wheelMouse()
	{
		if (isFocused || Mouse.getEventDWheel() == 0)
			return;
		mc.thePlayer.inventory.currentItem += Mouse.getEventDWheel() < 0 ? 1 : -1;
		if (mc.thePlayer.inventory.currentItem < 0)
			mc.thePlayer.inventory.currentItem = 8;
		if (mc.thePlayer.inventory.currentItem > 8)
			mc.thePlayer.inventory.currentItem = 0;
	}

	private void clickMouse(int button)
	{
		if (isFocused || button < 0)
			return;
		if (button == 0 && isLeftClickUp)
		{
			isLeftClickUp = false;
			mc.thePlayer.swingItem();
		}
		boolean flag = true;
		ItemStack itemstack = mc.thePlayer.inventory.getCurrentItem();
		if (mc.objectMouseOver == null)
			return;
		if (mc.objectMouseOver.typeOfHit == MovingObjectType.ENTITY)
		{
			if (button == 0)
				mc.playerController.attackEntity(mc.thePlayer, mc.objectMouseOver.entityHit);
			if (this.rightClickTimer == 0 && button == 1 && mc.playerController.interactWithEntitySendPacket(mc.thePlayer, mc.objectMouseOver.entityHit))
				flag = false;
		}
		else if (mc.objectMouseOver.typeOfHit == MovingObjectType.BLOCK)
		{
			int i = mc.objectMouseOver.blockX;
			int j = mc.objectMouseOver.blockY;
			int k = mc.objectMouseOver.blockZ;
			int i1 = mc.objectMouseOver.sideHit;
			if (button == 0)
			{
				if (mc.objectMouseOver != null && mc.objectMouseOver.typeOfHit == MovingObjectPosition.MovingObjectType.BLOCK)
				{
					if (mc.theWorld.getBlock(i, j, k).getMaterial() != Material.air)
					{
						mc.playerController.onPlayerDamageBlock(i, j, k, i1);

						if (mc.thePlayer.isCurrentToolAdventureModeExempt(i, j, k))
						{
							mc.effectRenderer.addBlockHitEffects(i, j, k, mc.objectMouseOver);
							mc.thePlayer.swingItem();
						}
					}
				}
			}
			else if (this.rightClickTimer == 0 && button == 1)
			{
				int j1 = itemstack != null ? itemstack.stackSize : 0;
				boolean result = !ForgeEventFactory.onPlayerInteract(mc.thePlayer, Action.RIGHT_CLICK_BLOCK, i, j, k, i1).isCanceled();
				if (result && mc.playerController.onPlayerRightClick(mc.thePlayer, mc.theWorld, itemstack, i, j, k, i1, mc.objectMouseOver.hitVec))
				{
					flag = false;
					mc.thePlayer.swingItem();
				}
				if (itemstack == null)
					return;
				if (itemstack.stackSize == 0)
					mc.thePlayer.inventory.mainInventory[mc.thePlayer.inventory.currentItem] = null;
				else if (itemstack.stackSize != j1 || mc.playerController.isInCreativeMode())
					mc.entityRenderer.itemRenderer.resetEquippedProgress();
			}
		}
		if (this.rightClickTimer == 0 && button == 1)
			this.rightClickTimer = 4;
		if (flag && button == 1)
		{
			ItemStack itemstack1 = mc.thePlayer.inventory.getCurrentItem();
			boolean result = !ForgeEventFactory.onPlayerInteract(mc.thePlayer, Action.RIGHT_CLICK_AIR, 0, 0, 0, -1).isCanceled();
			if (result && itemstack1 != null && mc.playerController.sendUseItem(mc.thePlayer, mc.theWorld, itemstack1))
				mc.entityRenderer.itemRenderer.resetEquippedProgress2();
		}
	}
	
	/**
	 * get move access in world
	 * 
	 * @author Dermenslof, Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 */
	protected boolean hasMoveControl()
	{
		return this.hasMoveControl;
	}

	/**
	 * set click access in world
	 * 
	 * @author Dermenslof, Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 */
	protected void setClickControl(boolean control)
	{
		this.hasClickControl = control;
	}
	
	/**
	 * get click access in world
	 * 
	 * @author Dermenslof, Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 */
	protected boolean hasClickControl()
	{
		return this.hasClickControl;
	}

	/**
	 * set move access in world
	 * 
	 * @author Dermenslof, Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 */
	protected void setMoveControl(boolean control)
	{
		this.hasMoveControl = control;
	}

	/**
	 * get if mouse is grab on Minecraft or not
	 * 
	 * @author Dermenslof, Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 */
	protected void setFocus(boolean focus, boolean change)
	{
		if (!Mouse.isInsideWindow())
			return;
		this.isFocused = focus;
		if (!change)
			return;
		this.mc.inGameHasFocus = !focus;
		if (focus)
			this.mc.mouseHelper.ungrabMouseCursor();
		else
		{
			Mouse.setCursorPosition(this.mc.displayWidth / 2, this.mc.displayHeight / 2);
			this.mc.mouseHelper.grabMouseCursor();
		}
	}
	
	/**
	 * get if mouse is grab on Minecraft or not
	 * 
	 * @author Dermenslof, Dren
	 * @since Phonecraft 1.0 for Minecraft 1.7.2
	 * @return true if mouse isn't grab on Minecraft
	 */
	public boolean getFocus()
	{
		return this.isFocused;
	}
}
