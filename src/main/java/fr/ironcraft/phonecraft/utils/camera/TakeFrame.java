package fr.ironcraft.phonecraft.utils.camera;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.IntBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.sound.midi.Sequencer;

import net.minecraft.client.Minecraft;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneCamera;

public class TakeFrame implements Runnable
{

	private int[] list;
	private GuiPhoneCamera gui;
	private Minecraft mc;
	private SequenceEncoder encoder;
	private boolean finish;

	public TakeFrame(Minecraft mc, GuiPhoneCamera gui, int[] list, SequenceEncoder encoder)
	{
		this(mc, gui, list, encoder, false);
	}

	public TakeFrame(Minecraft mc, GuiPhoneCamera gui, int[] list, SequenceEncoder encoder, boolean finish)
	{
		this.finish = finish;
		this.mc = mc;
		this.gui = gui;
		this.list = list;
		this.encoder = encoder;
	}

	@Override
	public void run()
	{
		copyList(this.list, this.mc.displayWidth, this.mc.displayHeight);
		BufferedImage bi = new BufferedImage(this.mc.displayWidth, this.mc.displayHeight, 1);
		bi.setRGB(0, 0, this.mc.displayWidth, this.mc.displayHeight, list, 0, this.mc.displayWidth);
		gui.hideGui = false;
		encoder.encodeImage(bi);
		if (this.finish)
		{
			try
			{
				encoder.finish();
				String var1 = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss").format(new Date()).toString();
				new File(this.mc.mcDataDir, Phonecraft.phoneFolder + "media/movies/test.mp4").renameTo(new File(this.mc.mcDataDir, Phonecraft.phoneFolder + "media/movies/" + var1 + ".mp4"));
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	private void copyList(int[] par0ArrayOfInteger, int par1, int par2)
	{
		int[] var1 = new int[par1];
		int var2 = par2 / 2;
		for (int var3 = 0; var3 < var2; ++var3)
		{
			System.arraycopy(par0ArrayOfInteger, var3 * par1, var1, 0, par1);
			System.arraycopy(par0ArrayOfInteger, (par2 - 1 - var3) * par1, par0ArrayOfInteger, var3 * par1, par1);
			System.arraycopy(var1, 0, par0ArrayOfInteger, (par2 - 1 - var3) * par1, par1);
		}
	}
}
