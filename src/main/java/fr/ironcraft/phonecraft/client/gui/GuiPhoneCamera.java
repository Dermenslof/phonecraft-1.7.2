package fr.ironcraft.phonecraft.client.gui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.client.KeyHandler;
import fr.ironcraft.phonecraft.common.blocks.ICBlocks;
import fr.ironcraft.phonecraft.utils.TextureUtils;
import fr.ironcraft.phonecraft.utils.camera.TakeScreenshot;
import fr.ironcraft.phonecraft.utils.camera.SequenceEncoder;
import fr.ironcraft.phonecraft.utils.camera.TakeFrame;
import fr.ironcraft.phonecraft.utils.camera.TaskFlash;

enum EnumState
{
	PHOTO, QRCODE, VIDEO
}

/**
 * @author Dermenslof, DrenBx
 */
@SideOnly(Side.CLIENT)
public class GuiPhoneCamera extends GuiPhoneInGame
{
	public static EnumState state = EnumState.PHOTO;
	public static List<int[]> lastBlocks = new ArrayList<int[]>();
	public static boolean isFlash;
	public static ResourceLocation textureFlash = new ResourceLocation(TextureUtils.getTextureNameForGui("icons/flash"));
	public static ResourceLocation textureqrCode = new ResourceLocation("phonecraft:textures/blocks/qrCode.png");
	public static ResourceLocation textureicon = new ResourceLocation("phonecraft:textures/gui/icons.png");
	public static boolean takeFrame;
	public static List<BufferedImage> frames = new ArrayList<BufferedImage>();
	private long lastFrameTime;
	public static SequenceEncoder encoder;
	private boolean stopMovie;
	private boolean isRecord;

	public GuiPhoneCamera(Minecraft par1Minecraft)
	{
		super(par1Minecraft);
		this.setClickControl(false);
	}
	
	public GuiPhoneCamera(Minecraft par1Minecraft, boolean anim)
	{
		this(par1Minecraft);
		if (!anim)
		{
			this.scale = 2.8F;
			this.screen = 1;
			this.angle = -90;
			this.mc.gameSettings.hideGUI = true;
		}
	}

	public void initGui()
	{
//		AudioFormat	audioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100.0F, 16, 2, 4, 44100.0F, false);
//
//		DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
//		SourceDataLine targetDataLine = null;
//		try
//		{
//			targetDataLine = (SourceDataLine) AudioSystem.getLine(info);
//			targetDataLine.open(audioFormat);
//			FloatControl control = (FloatControl)targetDataLine.getControl(FloatControl.Type.MASTER_GAIN);
//			control.setValue(-20.0F);
//			for (Control c : targetDataLine.getControls())
//				System.out.println(c.toString());
//			targetDataLine.close();
//		}
//		catch (LineUnavailableException e)
//		{
//			e.printStackTrace();
//		}
		
		this.animPhoto = true;
		super.initGui();
		this.encoder = new SequenceEncoder(new File(this.mc.mcDataDir, Phonecraft.phoneFolder + "media/movies/test.mp4"));
	}

	public void updateScreen()
	{
		super.updateScreen();
	}

	public boolean doesGuiPauseGame()
	{
		return false;
	}

	public void keyTyped(char par1, int par2)
	{
		if (par2 == 1)
		{
			this.mc.gameSettings.fovSetting = 0;
			this.mc.gameSettings.hideGUI = false;
			this.mc.displayGuiScreen(new GuiIngameMenu());
		}
		if (par1 == ' ' && !lastJump)
		{
			this.keyDelay = 8;
			this.keySpaceCounter++;
		}
		if (par2 == this.mc.gameSettings.keyBindForward.getKeyCode() && this.isUpRelease && this.mc.thePlayer.onGround)
		{
			this.isUpRelease = false;
			this.keyDelay = 8;
			this.keyUpCounter++;
		}
	}

	public void mouseClicked(int i, int j, int k)
	{
		super.mouseClicked(i, j, k);
		if(this.screen == 1 && !this.hideGui && !this.getFocus() && k == 0)
		{
			if (isFlash)
				this.setBlockFlash();
			this.hideGui = true;
			this.shootCamera = true;
		}
		else if (k == 0 && this.getFocus())
		{
			if (i >= this.width - 28 && i <= this.width - 12)
			{
				if (j >= 10 && j <= 55)
				{
					switch (state)
					{
					case PHOTO:
						this.encoder = new SequenceEncoder(new File(this.mc.mcDataDir, Phonecraft.phoneFolder + "media/movies/test.mp4"));
						state = EnumState.QRCODE;
						break;
					case QRCODE:
						state = EnumState.VIDEO;
						frames.clear();
						break;
					case VIDEO:
						this.stopMovie = true;
						state = EnumState.PHOTO;
					}
				}
			}
			else if (i >= 12 && i <= 28)
			{
				if (j >= 10 && j <= 40)
					isFlash = !isFlash;
			}
			else if (this.state == EnumState.VIDEO)
			{
				if (isRecord)
				{
					this.stopMovie = true;
					state = EnumState.PHOTO;
				}
				else
					this.isRecord = true;
			}
		}
	}

	public void handleMouseInput()
	{
		int x = Mouse.getEventX() * this.width / this.mc.displayWidth;
		int y = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;
		int w = Mouse.getDWheel();
		if (w != 0 && !this.getFocus())
		{
			if (w > 0)
				this.mc.gameSettings.fovSetting -= 0.1F;
			else if (w < 0)
				this.mc.gameSettings.fovSetting += 0.1F;
			if (this.mc.gameSettings.fovSetting > 0F)
				this.mc.gameSettings.fovSetting = 0F;
			else if (this.mc.gameSettings.fovSetting < -1.6F)
				this.mc.gameSettings.fovSetting = -1.6F;
		}
		super.handleMouseInput();
	}

	private void drawBackground()
	{
		GL11.glPushMatrix();
		//timing animation photo
		if(this.animPhoto)
		{
			this.screen = -1;
			this.changePoint += 10;
			if(this.changePoint >= this.width / 2)
			{
				this.changePoint = this.width / 2;
				this.scale += 0.1F;
				if(this.scale >= 4F)
				{
					this.scale = 4F;
					this.screen = 1;
					this.mc.gameSettings.hideGUI = true;
				}
			}
			this.angle -= 4;
			if(this.angle <= -90)
				this.angle = -90;
		}
		else
		{
			this.scale -= 0.1F;
			if(this.scale <= 1F)
			{
				this.scale = 1F;
				if(this.changePoint > 0)
					this.changePoint -= 10;
				if(this.changePoint <= 0)
					this.changePoint = 0;
				if(this.angle < 0)
					this.angle += 4;
				if(this.angle >= 0)
				{
					this.angle = 0;
					this.mc.gameSettings.hideGUI = false;
					this.mc.displayGuiScreen(new GuiPhoneMenu(this.mc, true));
				}
			}
		}
		GL11.glPopMatrix();
	}

	public void drawScreen(int par1, int par2, float par3)
	{
		if(this.screen != 1)
		{
			super.drawScreen(par1, par2, par3);
			drawBackground();
		}
		else
			drawCamera(par1, par2, par3);
	}

	private void drawCamera(int par1, int par2, float par3)
	{

		long now = System.currentTimeMillis();
		if (now - this.lastFrameTime >= 1000 / 10) // take frames at 10 fps
		{
			if (this.isRecord && (state == EnumState.VIDEO || this.stopMovie))
			{
				this.lastFrameTime = now;
				this.hideGui = true;
				IntBuffer buffer = null;
				int var2 = this.mc.displayWidth * this.mc.displayHeight;
				buffer = BufferUtils.createIntBuffer(var2);
				int[] list = new int[var2];
				buffer.clear();
				GL11.glReadPixels(0, 0, this.mc.displayWidth, this.mc.displayHeight, GL12.GL_BGRA, GL12.GL_UNSIGNED_INT_8_8_8_8_REV, buffer);
				buffer.get(list);
				if (this.stopMovie)
				{
					this.isRecord = false;
					this.stopMovie = false;
					new Thread(new TakeFrame(this.mc, this, list, this.encoder, true)).start();
				}
				else
					new Thread(new TakeFrame(this.mc, this, list, this.encoder)).start();
			}
		}
		if (this.isRecord && (state == EnumState.VIDEO || this.stopMovie))
		{
			if ((now / 1000) % 2 != 0)
				this.drawAbsoluteRoundedRect(15, 52, 15 + 10, 52 + 10, 5, 0xff0000, 1F);
		}
		if(this.shootCamera)
		{
			this.shootCamera = false;
			IntBuffer buffer = null;
			int[] list = null;
			int var2 = this.mc.displayWidth * this.mc.displayHeight;
			if (buffer == null || buffer.capacity() < var2)
			{
				buffer = BufferUtils.createIntBuffer(var2);
				list = new int[var2];
			}
			GL11.glPixelStorei(GL11.GL_PACK_ALIGNMENT, 1);
			GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);
			buffer.clear();
			GL11.glReadPixels(0, 0, this.mc.displayWidth, this.mc.displayHeight, GL12.GL_BGRA, GL12.GL_UNSIGNED_INT_8_8_8_8_REV, buffer);
			buffer.get(list);
			new Thread(new TakeScreenshot(this.mc, this, list, state == EnumState.QRCODE)).start();
			EntityPlayer p = this.mc.thePlayer;
			if (p.worldObj.isRemote)
				p.playSound("phonecraft:clicPhoto", 0.75F, 6F);
			else
				p.worldObj.playSoundAtEntity(p, "phonecraft:clicPhoto", 0.75F, 6F);
			if (isFlash)
				this.mc.displayGuiScreen(new TaskFlash(this.mc, 200));
		}
		if(Keyboard.isKeyDown(KeyHandler.key_PhoneGUI.getKeyCode()))
		{
			this.mc.gameSettings.fovSetting = 0;
			this.screen = -1;
			this.animPhoto = false;
			this.isCamera = false;
			this.changePoint = this.width/2;
			this.scale = 4F;
			this.angle = -90;
		}
//		if(!this.hideGui)
//		{
			if (lastBlocks.size() != 0)
			{
				for (int i=0; i<lastBlocks.size(); i++)
				{
					int[] loc = lastBlocks.get(i);
					this.mc.thePlayer.worldObj.setBlock(loc[0], loc[1], loc[2], Blocks.air, 0, 2);
				}
				lastBlocks.clear();
			}
			if (state != EnumState.QRCODE)
			{
				GL11.glPushMatrix();
				this.mc.renderEngine.bindTexture(this.getTexturePhone());
				GL11.glEnable(GL11.GL_BLEND);
				GL11.glColor4f(1,  1,  1,  1F);
				this.drawTexturedModalRect(this.width/2 - 35, this.height/2 - 23, 110, 25, 100, 50);
				GL11.glPopMatrix();
			}
			else
			{
				this.drawHorizontalLine(this.width / 2 - 100, this.width / 2 - 80, this.height / 2 - 100, 0xffffffff);
				this.drawHorizontalLine(this.width / 2 + 100, this.width / 2 + 80, this.height / 2 - 100, 0xffffffff);
				this.drawHorizontalLine(this.width / 2 - 100, this.width / 2 - 80, this.height / 2 + 100, 0xffffffff);
				this.drawHorizontalLine(this.width / 2 + 100, this.width / 2 + 80, this.height / 2 + 100, 0xffffffff);

				this.drawVerticalLine(this.width / 2 - 100, this.height / 2 - 100, this.height / 2 - 80, 0xffffffff);
				this.drawVerticalLine(this.width / 2 + 100, this.height / 2 - 100, this.height / 2 - 80, 0xffffffff);
				this.drawVerticalLine(this.width / 2 - 100, this.height / 2 + 100, this.height / 2 + 80, 0xffffffff);
				this.drawVerticalLine(this.width / 2 + 100, this.height / 2 + 100, this.height / 2 + 80, 0xffffffff);
			}
			GL11.glPushMatrix();
			double percentZoom = -this.mc.gameSettings.fovSetting * 100 / 1.6;
			float time = mc.theWorld.getCelestialAngle(1.0F);
			boolean night = time >= 0.765 || time < 0.245 ? false : true;
			GL11.glColor4f(1,  1,  1,  1F);
			this.drawAbsoluteRoundedRect(this.width - 30, this.height - 115, this.width - 10, this.height - 5, 5, 0, 0.8F);
			this.drawAbsoluteRect(this.width - 21, this.height - 110, this.width - 19, this.height - 10, 0x00757575, 0.8F);
			this.drawAbsoluteRect(this.width - 25, this.height - 12 - (int)percentZoom, this.width - 15, this.height - 8 - (int)percentZoom, 0x00b2b2b2, 1F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			int nb = state == EnumState.QRCODE ? 15 : (state == EnumState.VIDEO ? 30 : 0);
			this.drawAbsoluteRoundedRect(this.width - 30, 5, this.width - 10, 60, 5, night ? 0xff727272 : 0, night ? 1F : 0.8F);
			this.drawAbsoluteRect(this.width - 28, 10, this.width - 12, 55, night ? 0 : 0x757575, night ? 0.5F : 0.15F);
			this.drawAbsoluteRect(this.width - 28, 10 + nb, this.width - 12, 25 + nb, 0xffb2b2b2, 1F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glColor4f(1F, 1F, 1F, 1F);
			if (state == EnumState.QRCODE)
			{
				GL11.glScalef(0.0565F, 0.0565F, 0.0565F);
				this.mc.renderEngine.bindTexture(textureqrCode);
				this.drawTexturedModalRect((int)((this.width - 27)/0.0565F), (int)(25/0.0565F), 0, 0, 256, 256);
			}
			else if (state == EnumState.VIDEO)
				this.drawAbsoluteRoundedRect(this.width - 25, 42, this.width - 25 + 10, 42 + 10, 5, 0xff0000, 1F);
			else
			{
				this.mc.renderEngine.bindTexture(this.getTextureIcons());
				this.drawTexturedModalRect(this.width - 28, 10, 0, 16, 16, 16);
			}
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			int flash = isFlash ? 15 : 0;
			this.drawAbsoluteRoundedRect(10, 5, 30, 45, 5, night ? 0xff727272 : 0, night ? 1F : 0.8F);
			this.drawAbsoluteRect(12, 10, 28, 40, night ? 0 : 0x757575, night ? 0.5F : 0.15F);
			this.drawAbsoluteRect(12, 10 + flash, 28, 25 + flash, 0x00b2b2b2, 1F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			this.mc.renderEngine.bindTexture(this.getTextureIcons());
			GL11.glEnable(GL11.GL_BLEND);
			if (isFlash)
				this.drawTexturedModalRect(12, 25, 0, 96, 16, 16);
			else
				this.drawTexturedModalRect(12, 10, 16, 96, 16, 16);
			GL11.glPopMatrix();
//		}
//		else
//		{
			
//		}
	}

	private void setBlockFlash()
	{	
		int[] loc = {(int)this.mc.thePlayer.posX , (int)this.mc.thePlayer.posY - 1, (int)this.mc.thePlayer.posZ};
		this.lastBlocks.add(loc);
		this.mc.theWorld.setBlock(loc[0], loc[1], loc[2], ICBlocks.flash, 0, 2);;
	}
	
	@Override
	protected void onMouseOverPhone(int x, int y)
	{
		//A suppromer
		;
	}
}
