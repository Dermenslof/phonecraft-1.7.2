package apps.exemple.minebox;

import java.awt.image.BufferedImage;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.ironcraft.phonecraft.client.ClientProxy;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneInGame;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneMenu;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneNoNetWork;
import fr.ironcraft.phonecraft.client.gui.button.EnumPhoneButton;
import fr.ironcraft.phonecraft.client.gui.button.GuiPhoneButton;
import fr.ironcraft.phonecraft.client.gui.button.GuiPhoneButtonMenu;
import fr.ironcraft.phonecraft.utils.ImageLoader;
import fr.ironcraft.phonecraft.utils.TextUtils;

@SideOnly(Side.CLIENT)
public class GuiPhoneMinebox extends GuiPhoneInGame
{
	public BufferedImage albumImg = null;
	public BufferedImage prevAlbumImg = null;
	public String[] infos;
	public long delay;
	public long totalTime;
	public long backTime;
	public long tmp;
	public long start;

	private Thread json;
	private int texture;
	private int textureBack;
	private int textureLogo;
	private int textureUnknow;
	private int textureIcons;

	public static Minecraft mc = Minecraft.getMinecraft();
	private ImageLoader imageLoader;

	public GuiPhoneMinebox()
	{
		super(mc);
		this.imageLoader = new ImageLoader();
		this.needNetwork = true;
	}

	public void initGui()
	{
		this.buttonList.clear();
		super.initGui();
		
		//buttons;
		int[] ic = {51, 50, 66, 36};
		for (int t=0; t < 4; ++t)
		{
			GuiPhoneButton b = new GuiPhoneButtonMenu(this, t + 2, 1 + (t * 23), this.getScreenHeight() - 19, 23, 18, "", true);
			b.setIcon(this.getTextureIcons(), 3, 1, ic[t], 1F);
			this.buttonList.add(b);
		}
		
		if (this.needNetwork)
		{
			if (!this.hasNetwork())
			{
				this.mc.displayGuiScreen(new GuiPhoneNoNetWork(this.mc));
				return;
			}
		}
		this.textureBack = this.imageLoader.setupTexture("apps/exemple/minebox:textures/mbrBack.png");
		this.textureLogo = this.imageLoader.setupTexture("apps/exemple/minebox:textures/mbrLogo.png");
		this.textureUnknow = this.imageLoader.setupTexture("apps/exemple/minebox:textures/unknow.png");
		AppMinebox.sound = StreamSoundThread.getThread();
		if(!StreamSoundThread.isStart())
			AppMinebox.sound.start();
	}

	public void updateScreen()
	{
		super.updateScreen();
		if (this.needNetwork)
		{
			if (!this.hasNetwork())
			{
				if (StreamSoundThread.isStart())
					AppMinebox.sound.interrupt();
				this.mc.displayGuiScreen(new GuiPhoneNoNetWork(this.mc));
				return;
			}
		}
		long time = System.currentTimeMillis();
		if (backTime > 0)
			backTime = tmp - (time - start);
		if (this.infos != null)
		{
			if (time >= delay)
				getInfos();
		}
		else
			getInfos();
		if(AppMinebox.sound != null)
			AppMinebox.sound.updateSoundVolume();
	}
	
	protected void actionPerformed(GuiButton button)
	{
		super.actionPerformed(button);
		switch (button.id)
		{
		case 2:
			AppMinebox.sound.volume -= 0.05F;
			break;
		case 3:
			AppMinebox.sound.volume += 0.05F;
			break;
		case 4:
			getInfos();
			break;
		case 5:
			AppMinebox.sound.interrupt();
			this.mc.displayGuiScreen(new GuiPhoneMenu(this.mc));
			break;
		}
	}

	public void drawScreen(int par1, int par2, float par3)
	{
		boolean pub = false;
		boolean info = infos != null && infos.length > 0;
		if (infos != null)
			pub = !infos[2].toLowerCase().contains("adver") && !infos[2].toLowerCase().contains("mineboxradio") && !infos[1].startsWith("#");
		super.drawScreen(par1, par2, par3);
		drawBackground();
		//infos
		if(info)
		{
			if (pub)
				drawInfos();
			else
				this.drawStringOnScreen("Pub", this.getScreenWidth() / 2 - this.getFont().getStringWidth("Pub") / 2, 31,  0xd2d2d2, this.transparency);
		}
		else
			drawWaiting();
		if (info && pub)
			drawTimeLine();
		drawButtons(par1, par2);
		onMouseOverPhone(par1, par2);
	}

	private void drawBackground()
	{
		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glColor4f(1, 1, 1, this.transparency);
		GL11.glEnable(GL11.GL_TEXTURE_2D);

		//background
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.textureBack);
		this.drawImageOnScreen(0, 0, 0, 0, this.getScreenWidth(), this.getScreenHeight());

		//logo
		GL11.glScalef(0.34F,  0.48F,  1);
		GL11.glTranslatef((this.getScreenPosX() / 0.34F), this.getScreenPosY() / 0.48F, 1.0F);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.textureLogo);
		this.drawTexturedModalRect(10, 0, 0, 0, 235, 57);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();

		//cadres
		this.drawRect(6, 28, this.getScreenWidth() - 6, 55, 0x0, -0.75F);
	}

	private void drawInfos()
	{
		GL11.glPushMatrix();
		GL11.glScalef(1F, 1F,  1);
		this.drawRect(this.getScreenWidth() / 2 - 24, 59, this.getScreenWidth() / 2 + 24, 107, 0x00, -0.4F);
		this.drawRoundedRect(2, this.getScreenHeight() - 38, this.getScreenWidth() - 2, this.getScreenHeight() - 21, 3, 0x00, -0.5F);

		GL11.glTranslatef(this.getScreenPosX() + 8, this.getScreenPosY(), 0);
		GL11.glScalef(0.5F, 0.5F, 1);
		String[] names = infos[2].split(" - ");
		this.drawString("Artiste(s):" , 0, 0 + 54,  0xd2d2d2, this.transparency);
		for(int i=0; i<names.length; i++)
			this.drawString(names[i] , 50, 10 * i + 54,  0xd2d2d2, this.transparency);
		this.drawString(infos[1] , 0, 92,  0xd2d2d2, this.transparency);
		GL11.glPopMatrix();
		drawJacket();
	}

	private void drawJacket()
	{
		if (this.albumImg != null)
		{
			if (this.prevAlbumImg != this.albumImg)
			{
				this.prevAlbumImg = this.albumImg;
				this.texture = ClientProxy.imageLoader.setupTexture(albumImg);
			}
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.texture);
		}
		else
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.textureUnknow);
		GL11.glPushMatrix();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, this.transparency);
		GL11.glTranslatef(getScreenPosX() + getScreenWidth() / 2 - 19, getScreenPosY() + 64, 0);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glScalef(0.15F, 0.15F, 1F);
		this.drawTexturedModalRect(0, 0, 0, 0, 256, 256);
		GL11.glPopMatrix();
	}

	private void drawWaiting()
	{
		this.drawRect(22, 61, 71, 109, 0x00, -1.4F);
		String title = I18n.format("gui.minebox.loading", new Object[0]);
		this.drawString(title, this.getScreenPosX() + this.getScreenWidth() / 2 - this.getFont().getStringWidth(title) / 2, this.getScreenPosY() + 32, 0xd2d2d2);
		long t = System.currentTimeMillis() % 1000 / 100;
		String str = "";

		if (t < 2)
			str = "";
		else if (t < 4)
			str = ".";
		else if (t < 6)
			str = "..";
		else if (t < 8)
			str = "...";
		else
			str = "....";
		this.drawString(str, this.getScreenPosX() + 40, this.getScreenPosY() + this.getScreenHeight() / 2 - 2,  0xd2d2d2);
	}

	private void drawTimeLine()
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, this.transparency);
		long last = totalTime - backTime;
		int endOfLine = this.getScreenWidth() - 6;
		double percent = totalTime < 1 ? 0 : (((last)*100D)/totalTime);

		int barPosY = this.getScreenHeight() - 28;

		this.drawRect(6, barPosY, endOfLine, barPosY + 1, 0x232323, 0);

		int current = (int)(((endOfLine - 6) / 100D)*percent) < 0 ? 0 : (int)(((endOfLine - 6 )/ 100D) * percent);
		this.drawRect(6, barPosY, 6 + current, barPosY + 1, 0xa21122, 0);
		this.drawRoundedRect(5 + current, barPosY - 2, 7 + current, barPosY + 3, 1, 0xffffff, 0);
		GL11.glPushMatrix();
		GL11.glScalef(0.5F, 0.5F, 1);
		int minutes = last < 0 ? 0 : (int)(last/60);
		int seconds = last < 0 ? 0 : (int)((last -((last/1000)-minutes))/1000) % 60;
		this.getFont().drawString(this, "" + (minutes/1000) + ":" + (seconds < 10 ? "0" + seconds : (seconds == 60 ? "00" : "" + seconds)), (int)(this.getScreenPosX() / 0.5F + 13), (int)((this.height - 78) / 0.5F), 0xffffff, this.transparency);
		GL11.glPopMatrix();
	}
	
	private void drawButtons(int par1, int par2)
	{
		//buttons background
		GL11.glPushMatrix();
		GL11.glColor4f(1, 1, 1, this.transparency);
		this.drawGradientRect(0, this.getScreenHeight() - 20, this.getScreenWidth(), this.getScreenHeight(), 0x323232, 0x111111, 0F);
		GL11.glPopMatrix();
		for (Object b: this.buttonList)
			((GuiButton)b).drawButton(mc, par1, par2);
	}

	private void getInfos()
	{
		json = new Thread(new MbrThread(this));
		try
		{
			json.start();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}