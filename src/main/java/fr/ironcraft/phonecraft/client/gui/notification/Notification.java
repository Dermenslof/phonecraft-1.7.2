package fr.ironcraft.phonecraft.client.gui.notification;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.GregorianCalendar;

import fr.ironcraft.phonecraft.client.gui.button.GuiPhoneButton;
import fr.ironcraft.phonecraft.client.gui.button.GuiPhoneButtonLink;

public class Notification
{
	private EnumPriority priority;
	private Date date;
	private String title;
	private EnumType type;
	private boolean init;
	private String id;
	private double progress;
	private boolean isDead;
	private GuiPhoneButtonLink link = null;
	
	public Notification(EnumPriority priority, String title, EnumType type, String link)
	{
		if (link != null)
			this.link = new GuiPhoneButtonLink(-10, 0, 0, 10, 7, "link", link);
		this.date = GregorianCalendar.getInstance().getTime();
		this.priority = priority;
		this.title = title;
		this.type = type;
	}
	
	public Notification(EnumPriority priority, String title, EnumType type)
	{
		this(priority, title, type, null);
	}
	
	public String getID()
	{
		if (init)
			return null;
		init = true;
		this.id = null;
		byte[] thedigest = null;
		try
		{
			byte[] bytesOfMessage = ("" + System.currentTimeMillis()).getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance("MD5");
			thedigest = md.digest(bytesOfMessage);
			this.id = thedigest.toString();
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		return (this.id);
	}
	
	public void setTitle(String title, String id)
	{
		if (!compareID(id))
			return;
		this.title = title;
		this.date = GregorianCalendar.getInstance().getTime();
	}
	
	public void setDead(String id)
	{
		if (!compareID(id))
			return;
		this.isDead = true;
	}
	
	public void setType(EnumType type, String id)
	{
		if (!compareID(id))
			return;
		this.type = type;
	}
	
	public void setProgress(double progress, String id)
	{
		if (!compareID(id))
			return;
		this.progress = progress;
		this.date = GregorianCalendar.getInstance().getTime();
	}
	
	public boolean canDestroy()
	{
		switch (this.type)
		{
		case LINK:
			return this.link != null && !this.link.isOver;
		case INFO:
			return true;
		case PROGRESS:
			return this.progress == 100; 
		default:
			return this.isDead;
		}
	}
	
	public boolean compareID(String id)
	{
		return this.id.equals(id);
	}
	
	public String getTitle()
	{
		return this.title;
	}
	
	public EnumType getType()
	{
		return this.type;
	}
	
	public GuiPhoneButton getLink()
	{
		return this.link;
	}
	
	public double getProgress()
	{
		return this.progress;
	}
	
	public Date getDate()
	{
		return this.date;
	}
}
