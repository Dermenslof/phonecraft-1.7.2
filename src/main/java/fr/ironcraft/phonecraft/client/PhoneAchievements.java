package fr.ironcraft.phonecraft.client;

import java.awt.List;
import java.util.ArrayList;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Achievement;
import net.minecraftforge.common.AchievementPage;

/**
 * @author Dermenslof, DrenBx
 */
public class PhoneAchievements
{
	public AchievementPage phonePage;
	public static Achievement openPhone = (new Achievement("achievement.openPhonecraft", "openPhone", 0, 0, Items.book, (Achievement)null)).initIndependentStat().registerStat();
	
	public void init()
	{
		phonePage = new AchievementPage("Phonecraft",  openPhone);
		AchievementPage.registerAchievementPage(phonePage);
	}
}