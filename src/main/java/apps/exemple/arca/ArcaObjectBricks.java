package apps.exemple.arca;

import fr.ironcraft.phonecraft.client.gui.GuiPhoneInGame;

public class ArcaObjectBricks extends ArcaObject
{
	public ArcaObjectBricks(int id, GuiPhoneInGame gui, int posX, int posY, int width, int height)
	{
		super(id, gui, posX, posY, width, height, 0xffffff);
	}
	
	protected void drawObject()
	{
		if (this.enabled)
		{
			super.drawObject();
			gui.drawRect((int)posX, (int)posY, (int)posX + width, (int)posY + 1, 0x757575, 0);
			gui.drawRect((int)posX, (int)posY, (int)posX + 1, (int)posY + height, 0x757575, 0);
			gui.drawRect((int)posX + width - 1, (int)posY, (int)posX + width, (int)posY + height, 0x323232, 0);
			gui.drawRect((int)posX, (int)posY + height - 1, (int)posX + width, (int)posY + height, 0x323232, 0);
			
			gui.drawRect((int)posX, (int)posY + height - 1, (int)posX + 1, (int)posY + height, 0x555555, 0);
			gui.drawRect((int)posX + width - 1, (int)posY, (int)posX + width, (int)posY + 1, 0x555555, 0);
		}
	}
}
