package fr.ironcraft.phonecraft.utils.camera;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;

import org.jcodec.codecs.h264.H264Encoder;
import org.jcodec.codecs.h264.H264Utils;
import org.jcodec.common.NIOUtils;
import org.jcodec.common.SeekableByteChannel;
import org.jcodec.common.model.ColorSpace;
import org.jcodec.common.model.Picture;
import org.jcodec.containers.mp4.Brand;
import org.jcodec.containers.mp4.MP4Packet;
import org.jcodec.containers.mp4.TrackType;
import org.jcodec.containers.mp4.muxer.FramesMP4MuxerTrack;
import org.jcodec.containers.mp4.muxer.MP4Muxer;
import org.jcodec.scale.AWTUtil;
import org.jcodec.scale.RgbToYuv420;

public class SequenceEncoder
{
	private SeekableByteChannel ch;
	private Picture toEncode;
	private RgbToYuv420 transform;
	private H264Encoder encoder;
	private ArrayList<ByteBuffer> spsList;
	private ArrayList<ByteBuffer> ppsList;
	private FramesMP4MuxerTrack outTrack;
	private ByteBuffer _out;
	private int frameNo;
	private MP4Muxer muxer;

	public SequenceEncoder(File out)
	{
		try
		{
			this.ch = NIOUtils.writableFileChannel(out);
			muxer = new MP4Muxer(ch, Brand.MOV);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		transform = new RgbToYuv420(0, 0);
		
		outTrack = muxer.addTrackForCompressed(TrackType.VIDEO, 10);
		_out = ByteBuffer.allocate(1920 / 4 * 1080 / 4 * 6);
		encoder = new H264Encoder();
		spsList = new ArrayList<ByteBuffer>();
		ppsList = new ArrayList<ByteBuffer>();
	}

	public void encodeImage(BufferedImage bi)
	{
		if (toEncode == null)
			toEncode = Picture.create(bi.getWidth(), bi.getHeight(), ColorSpace.YUV420);
		for (int i = 0; i < 3; i++)
			Arrays.fill(toEncode.getData()[i], 0);
		transform.transform(AWTUtil.fromBufferedImage(bi), toEncode);
		_out.clear();
		ByteBuffer result = encoder.encodeFrame(_out, toEncode);
		spsList.clear();
		ppsList.clear();
		H264Utils.encodeMOVPacket(result, spsList, ppsList);
		try
		{
			outTrack.addFrame(new MP4Packet(result, frameNo, 10, 1, frameNo, true, null, frameNo, 0));
			frameNo++;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void finish() throws IOException
	{
		outTrack.addSampleEntry(H264Utils.createMOVSampleEntry(spsList, ppsList));
//		muxer.addUncompressedAudioTrack(format);
		muxer.writeHeader();
		NIOUtils.closeQuietly(ch);
	}
}
