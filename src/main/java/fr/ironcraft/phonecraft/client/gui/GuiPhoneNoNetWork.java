package fr.ironcraft.phonecraft.client.gui;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import fr.ironcraft.phonecraft.client.AppRegistry;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneInGame;
import fr.ironcraft.phonecraft.utils.TextUtils;

public class GuiPhoneNoNetWork extends GuiPhoneInGame
{

	public GuiPhoneNoNetWork(Minecraft par1Minecraft)
	{
		super(par1Minecraft);
	}

	@Override
	public void initGui()
	{
		super.initGui();
	}

	@Override
	public void drawScreen(int par1, int par2, float par3)
	{
		super.drawScreen(par1, par2, par3);
		String[] lines = TextUtils.getLanguage("gui.phone.networkrequire").split(" ");
		GL11.glPushMatrix();
		GL11.glScalef(0.5F, 0.5F, 1F);
		for (int i=0; i<lines.length; ++i)
			this.drawString(lines[i], (int)((this.getScreenPosX() + this.getScreenWidth() / 2 - this.getFont().getStringWidth(lines[i]) / 4) / 0.5F), (int)((this.getScreenPosY() + 30 + (i * 10))/0.5F), 0xffffffff, 1F);
		GL11.glPopMatrix();
	}

	@Override
	public void handleMouseInput()
	{
		super.handleMouseInput();
		int x = Mouse.getEventX() * this.width / this.mc.displayWidth;
		int y = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;
	}

	@Override
	public void mouseClicked(int i, int j, int k)
	{
		super.mouseClicked(i, j, k);
	}

	protected void onMouseOverPhone(int x, int y)
	{
		super.onMouseOverPhone(x, y);
	}
}
