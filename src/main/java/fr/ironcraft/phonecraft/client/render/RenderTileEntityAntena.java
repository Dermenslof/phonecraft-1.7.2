package fr.ironcraft.phonecraft.client.render;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

import fr.ironcraft.phonecraft.model.ModelRelayTelecom;
import fr.ironcraft.phonecraft.model.ModelParabole;
import fr.ironcraft.phonecraft.model.ModelPied;

/**
 * @authors Dermenslof, DrenBx
 */
public class RenderTileEntityAntena extends TileEntitySpecialRenderer
{
    private final ModelParabole model;
    private final ModelPied model1;
    public static RenderTileEntityAntena instance;
    
    public RenderTileEntityAntena()
    {
            this.model = new ModelParabole();
            this.model1 = new ModelPied();
            instance = this;
    }
    
    private void adjustRotatePivotViaMeta(World world, int x, int y, int z)
    {
            int meta = world.getBlockMetadata(x, y, z);
            GL11.glPushMatrix();
            GL11.glRotatef(meta * (-90), 0.0F, 0.0F, 1.0F);
            GL11.glPopMatrix();
    }
    
    @Override
    public void renderTileEntityAt(TileEntity te, double x, double y, double z, float scale)
    {
            GL11.glPushMatrix();
            GL11.glTranslatef((float) x + 0.5F, (float) y + 1F, (float) z + 0.5F);
            GL11.glScalef(2, 2, 2);
            GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
            ResourceLocation textures = (new ResourceLocation("phonecraft:textures/blocks/relayTelecom_x4.png")); 
            Minecraft.getMinecraft().renderEngine.bindTexture(textures);
            GL11.glPushMatrix();
            GL11.glRotatef(((System.currentTimeMillis()) % 2000) * 360 / 2000, 0.0F, 1.0F, 0.0F);
            GL11.glRotatef(-30, 1F, 0.0F, 0.0F);
            this.model.render((Entity)null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
            GL11.glPopMatrix();
            GL11.glTranslatef(0, -1, 0);
            this.model1.render((Entity)null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
            GL11.glPopMatrix();
    }
    
    public void renderTileEntityAtItem(TileEntity te, double x, double y, double z, float scale)
    {
    	 GL11.glPushMatrix();
         GL11.glTranslatef((float) x + 0.5F, (float) y + 0.5F, (float) z + 0.5F);
         ResourceLocation textures = (new ResourceLocation("phonecraft:textures/blocks/relayTelecom_x4.png")); 
         Minecraft.getMinecraft().renderEngine.bindTexture(textures);
         GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
         GL11.glRotatef(0F, 0.0F, 1.0F, 0.0F);
         GL11.glPushMatrix();
         this.model.render((Entity)null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
         GL11.glPopMatrix();
         GL11.glTranslatef(0, -1, 0);
         this.model1.render((Entity)null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
         GL11.glPopMatrix();
    }

    private void adjustLightFixture(World world, int i, int j, int k, Block block)
    {
            Tessellator tess = Tessellator.instance;
            float brightness = block.getLightValue(world, i, j, k);
            int skyLight = world.getLightBrightnessForSkyBlocks(i, j, k, 0);
            int modulousModifier = skyLight % 65536;
            int divModifier = skyLight / 65536;
            tess.setColorOpaque_F(brightness, brightness, brightness);
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit,  (float) modulousModifier,  divModifier);
    }
}
