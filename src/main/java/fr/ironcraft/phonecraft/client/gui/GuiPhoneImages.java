package fr.ironcraft.phonecraft.client.gui;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.client.gui.button.EnumPhoneButton;
import fr.ironcraft.phonecraft.client.gui.button.GuiPhoneButton;
import fr.ironcraft.phonecraft.client.gui.button.GuiPhoneButtonMenu;
import fr.ironcraft.phonecraft.utils.ImageLoader;

/**
 * @author Dermenslof, DrenBx
 */
@SideOnly(Side.CLIENT)
public class GuiPhoneImages extends GuiPhoneInGame
{
	protected int prevImage;
	protected List<String> photos;
	protected BufferedImage buffered;
	protected boolean menuPhoto;
	protected ImageLoader imgLoader;
	protected static int image;
	protected int texture;
	protected Map<Integer, String> appsList = new HashMap<Integer, String>();
	protected static boolean rotate;

	public GuiPhoneImages (Minecraft par1Minecraft)
	{
		super(par1Minecraft);
		this.imgLoader = new ImageLoader();
	}

	public void initGui()
	{
		this.buttonList.clear();
		super.initGui();
		
		//buttons;
		int[] ic = {64, 80, 66, 65};
		for (int t=0; t < 4; ++t)
		{
			GuiPhoneButton b = new GuiPhoneButtonMenu(this, t + 2, 1 + (t * 23), this.getScreenHeight() - 19, 23, 18, "", true);
			b.setIcon(this.getTextureIcons(), 3, 1, ic[t], 1F);
			this.buttonList.add(b);
		}
		this.setFocus(true, false);
		this.screen = 4;
		this.menuPhoto = true;
		this.photos = getImagesAvaiable();
		this.prevImage = -1;
	}

	public boolean doesGuiPauseGame()
	{
		return false;
	}

	@Override
	protected void actionPerformed(GuiButton button)
	{
		super.actionPerformed(button);
		switch (button.id)
		{
		case 2:
			if(this.image != -1)
				this.mc.displayGuiScreen(new GuiPhoneEditImg(this.mc, new File(this.photos.get(this.image))));
			break;
		case 3:
			this.screen = 2;
			break;
		case 4:
			this.rotate = !this.rotate;
			break;
		case 5:
			try
			{
				String name = this.photos.get(this.image);
				File f = new File(this.mc.mcDataDir, Phonecraft.phoneFolder + "media/pictures/dcim/" + name);
				f.delete();
				this.photos = getImagesAvaiable();

				if(this.image >this.photos.size()-1)
					this.image--;
			}
			catch(Exception e)
			{
				this.image = 0;	
			}
			break;
		}
	}
	
	public void mouseClicked(int i, int j, int k)
	{
		super.mouseClicked(i, j, k);
		if (this.getFocus())
		{
			if(k == 1 && i >= getScreenPosX() && i <= getScreenPosX() + getScreenWidth())
			{
				if(j >= getScreenPosY() && j <= getScreenPosY() + getScreenHeight() - 20)
				{
					this.menuPhoto = !this.menuPhoto;
					boolean hideButtons = isHome && !isAnimated;
					for (Object b: this.buttonList)
					{
						if (((GuiButton)b).id >= 2 && ((GuiPhoneButton)b).id <= 5)
							((GuiButton)b).visible = !((GuiButton)b).visible;
					}
				}
			}
		}
	}

	public void drawScreen(int par1, int par2, float par3)
	{
		super.drawScreen(par1, par2, par3);
		drawImages(par1, par2, par3);
		//buttons background
		if (this.menuPhoto)
		{
			GL11.glPushMatrix();
			GL11.glColor4f(1, 1, 1, this.transparency);
			this.drawGradientRect(0, this.getScreenHeight() - 20, this.getScreenWidth(), this.getScreenHeight(), 0x323232, 0x111111, 0F);
			GL11.glPopMatrix();
			for (Object b: this.buttonList)
				((GuiButton)b).drawButton(mc, par1, par2);
		}
		onMouseOverPhone(par1, par2);
	}

	public void drawImages(int par1, int par2, float par3)
	{
		this.drawRect(0, 0, getScreenWidth(), getScreenHeight(), 0xff000000, 0);
		boolean doPop = false;
		boolean noImg = false;
		GL11.glPushMatrix();
		try
		{
			GL11.glEnable(GL11.GL_BLEND);
			String name = this.photos.get(this.image);
			File f = new File(this.mc.mcDataDir, Phonecraft.phoneFolder + "media/pictures/dcim/" + name);
			if(f.exists())
			{
				int sizeX = 256;
				int sizeY = 256;
				if(this.prevImage != this.image)
				{
					this.prevImage = this.image;
					Image image = ImageIO.read(f);
					this.buffered = (BufferedImage) image;
					this.texture = this.imgLoader.setupTexture(this.buffered);
				}
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.texture);
				if(this.rotate)
				{
					GL11.glTranslatef(this.getScreenPosX() + this.getScreenWidth(), this.getScreenPosY(), 0);
					GL11.glRotatef(90F, 0, 0, 1);
					GL11.glScalef(0.579F, 0.3600256F, 1);
				}
				else
				{
					GL11.glTranslatef(this.getScreenPosX() + 0.7F, this.getScreenPosY() + 40F + (this.menuPhoto ? 0 : 5), 0);
					GL11.glScalef(0.354F, 0.21230769F, 1);
				}
				GL11.glColor4f(1,  1,  1,  this.transparency);
				this.drawTexturedModalRect(0, 0, 0, 0, sizeX, sizeY);
			}
			else
				noImg = true;
			doPop = true;
		}
		catch(Exception ex)
		{
			doPop = noImg = true;
		}
		if(noImg)
		{
			this.drawStringOnScreen("pas d'image", this.getFont().getStringWidth("pas d'image") / 3, 20, 0xffd2d2d2, this.transparency);
			this.image = -1;
		}
		if(doPop)
			GL11.glPopMatrix();
		if(this.menuPhoto)
		{
			//barre head
			if(this.photos.size() != 0)
			{
				GL11.glPushMatrix();
				drawRect(0, 0, 92, 7, 0xff222222, 1.0F);
				GL11.glScalef(0.5F, 0.5F, 1);
				String str = String.valueOf(this.image+1)+"/"+String.valueOf(this.photos.size());
				this.drawString(str, (int) ((this.getScreenPosX() + this.getScreenWidth() / 2 - this.getFont().getStringWidth(str) / 4)/0.5F), (int)((this.getScreenPosY() - 1) / 0.5F), 0xffd2d2d2, this.transparency);
				GL11.glPopMatrix();
			}
		}
	}

	public List<String> getImagesAvaiable()
	{
		List<String> list = new ArrayList<String>();
		File dir = new File(this.mc.mcDataDir, Phonecraft.phoneFolder + "media/pictures/dcim/");
		if (!dir.exists())
			dir.mkdirs();
		for(File f : dir.listFiles())
		{
			if(f.getPath().contains(".png"))
				list.add(f.getName());
		}
		return list;
	}

	protected void onMouseOverPhone(int x, int y)
	{
		super.onMouseOverPhone(x, y);
		if(this.isTactile && !this.mouseIsDrag && this.clickX >= 0)
		{
			if(-this.clickX+this.releaseX >40)
				this.image--;
			else if(-this.clickX+this.releaseX <-40)
				this.image++;
			this.isTactile = false;
			this.clickX = this.releaseX = this.releaseY = this.clickY = -1;
		}
		if(this.image < 0)
			this.image = this.photos.size()-1;
		if(this.image > this.photos.size()-1)
			this.image = 0;
	}
}
