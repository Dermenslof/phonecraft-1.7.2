package fr.ironcraft.phonecraft.common.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import cpw.mods.fml.common.registry.GameRegistry;
import fr.ironcraft.phonecraft.client.CreaTabs;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityAntena;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityQrCode;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityRelayTelecom;
import fr.ironcraft.phonecraft.utils.TextureUtils;

/**
 * @authors Dermenslof, DrenBx
 */
public class ICBlocks
{
	public static Block qrCode, relayTelecom, flash, antena;
	
	public void init()
	{
		this.configBlocks();
		this.registerBlocks();
	}

	private void registerBlocks()
	{
		GameRegistry.registerBlock(qrCode, "qrCode");
		GameRegistry.registerBlock(relayTelecom, "relayTelecom");
		GameRegistry.registerBlock(flash, "flash");
		GameRegistry.registerBlock(antena, "antena");
		GameRegistry.registerTileEntity(TileEntityQrCode.class, "QrCode");
		GameRegistry.registerTileEntity(TileEntityRelayTelecom.class, "RelayTelecom");
		GameRegistry.registerTileEntity(TileEntityAntena.class, "Antena");
	}

	private void configBlocks()
	{
		qrCode = new BlockQrCode().setBlockName("qrCode").setBlockTextureName(TextureUtils.getTextureNameForBlocks("qrCode")).setStepSound(Block.soundTypeCloth).setCreativeTab(CreaTabs.phoneTab).setBlockUnbreakable();
		relayTelecom = new BlockRelayTelecom().setBlockName("relayTelecom").setBlockTextureName("iron_block").setStepSound(Block.soundTypeMetal).setCreativeTab(CreaTabs.phoneTab);
		flash = new BlockFlash(Material.glass).setHardness(0.3F).setLightLevel(1.0F).setBlockName("flash").setBlockTextureName(TextureUtils.getTextureNameForBlocks("flash"));
		antena = new BlockAntena().setBlockName("antena").setBlockTextureName("iron_block").setStepSound(Block.soundTypeMetal).setCreativeTab(CreaTabs.phoneTab);
	}
}
