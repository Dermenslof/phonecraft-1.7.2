package fr.ironcraft.phonecraft.utils.http;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.client.gui.GuiQrCodeEdit;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityQrCode;

@SideOnly(Side.CLIENT)
public class TryOpenGui implements Runnable
{
	private TileEntityQrCode tile;
	private String username;

	@SideOnly(Side.CLIENT)
	public TryOpenGui(TileEntityQrCode par0, String par1)
	{
		this.tile = par0;
		this.username = par1;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void run()
	{
		Minecraft mc = FMLClientHandler.instance().getClient();
		String result = "";
		/*DEBUG*/
		username = "Dermenslof";
		try
		{
			URL url = new URL(Phonecraft.urlFiles + "ops.txt");
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			String line;
			while ((line = in.readLine()) != null)
				result += "\n" + line;
			in.close();
		}
		catch(Exception ex){}

		if(!result.equals(""))
		{
			String[] names = result.split("\n");
			for (String name : names)
			{
				if (name.equals(username))
				{
					mc.displayGuiScreen(new GuiQrCodeEdit(mc, tile));
					break;
				}
			}
		}
		else
			mc.thePlayer.sendChatMessage(I18n.format("error.permission", new Object[0]));
	}
}
