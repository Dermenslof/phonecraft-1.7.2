package fr.ironcraft.phonecraft.client.gui.button;

import org.lwjgl.opengl.GL11;

import fr.ironcraft.phonecraft.client.gui.GuiPhoneInGame;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;

public class GuiPhoneButton extends GuiButton
{
	protected GuiPhoneInGame gui;
	protected ResourceLocation img;
	protected boolean hasIcon;
	protected int iconX;
	protected int iconY;
	protected boolean customTexture;
	protected int texture;
	protected int textureOffsetX;
	protected int textureOffsetY;
	protected int textureWidth;
	protected int textureHeight;
	protected float textureScale;
	
    public GuiPhoneButton(GuiPhoneInGame gui, int id, int x, int y, String title)
    {
        this(gui, id, x, y, 23, 23, title);
    }
    
	public GuiPhoneButton(GuiPhoneInGame gui, int id, int x, int y, int width, int height, String title)
	{
		super(id, x, y, width, height, title);
		this.gui = gui;
		this.width = width;
        this.height = height;
        this.xPosition = x + gui.getScreenPosX();
        this.yPosition = y + gui.getScreenPosY();
	}
	
	public void setIcon(ResourceLocation img, int x, int y, int id)
	{
		this.setIcon(img, x, y, id, 1F);
	}
	
	public void setIcon(ResourceLocation img, int x, int y, int id, float scale)
	{
		this.setIcon(img, x, y, (id % 16) * 16, (id / 16) * 16, 16, 16, scale);
	}
	
	public void setIcon(ResourceLocation img, int x, int y, int offX, int offY, int width, int height, float scale)
	{
		this.textureScale = scale;
		this.img = img;
		this.iconX = x;
		this.iconY = y;
		this.textureOffsetX = offX;
		this.textureOffsetY = offY;
		this.textureWidth = width;
		this.textureHeight = height;
		hasIcon = true;
	}
	
	public void setIcon(int img, int x, int y, int offX, int offY, int width, int height, float scale)
	{
		this.textureScale = scale;
		this.texture = img;
		this.iconX = x;
		this.iconY = y;
		this.textureOffsetX = offX;
		this.textureOffsetY = offY;
		this.textureWidth = width;
		this.textureHeight = height;
		hasIcon = true;
		customTexture = true;
	}
	
	public boolean hasIcon()
	{
		return this.hasIcon;
	}
	
	public void setPosition(int x, int y)
	{
		this.xPosition = x;
		this.yPosition = y;
	}
	
    public void drawButton(Minecraft p_146112_1_, int p_146112_2_, int p_146112_3_)
    {
    	this.enabled = true;
    	if ((this.gui.taskbarEnable || this.gui.taskbarSelected))
    		this.enabled = false;
        if (this.visible)
        {
            FontRenderer fontrenderer = p_146112_1_.fontRenderer;
            GL11.glColor4f(1.0F, 1.0F, 1.0F, gui.getTransparency());
            this.field_146123_n = p_146112_2_ >= this.xPosition && p_146112_3_ >= this.yPosition && p_146112_2_ < this.xPosition + this.width && p_146112_3_ < this.yPosition + this.height;
            int k = this.getHoverState(this.field_146123_n);
            if (hasIcon())
            {
            	GL11.glPushMatrix();
            	GL11.glEnable(GL11.GL_BLEND);
            	GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            	if (customTexture)
            		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture);
            	else
            		gui.mc.renderEngine.bindTexture(img);
            	GL11.glTranslatef(this.xPosition + iconX, this.yPosition + iconY, 0);
            	GL11.glScalef(textureScale, textureScale, 1F);
            	this.drawTexturedModalRect(0, 0, textureOffsetX, textureOffsetY, textureWidth, textureHeight);
            	GL11.glDisable(GL11.GL_BLEND);
            	GL11.glPopMatrix();
            }
            this.mouseDragged(p_146112_1_, p_146112_2_, p_146112_3_);
            int l = 14737632;

            if (packedFGColour != 0)
                l = packedFGColour;
            else if (!this.enabled)
                l = 10526880;
            else if (this.field_146123_n)
                l = 16777120;

            this.drawCenteredString(fontrenderer, this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, l);
        }
    }
    
    public void func_146113_a(SoundHandler p_146113_1_)
    {
//        p_146113_1_.playSound(PositionedSoundRecord.func_147674_a(new ResourceLocation("gui.button.press"), 1.0F));
    }
    
    /**
     * Fired when the mouse button is released. Equivalent of MouseListener.mouseReleased(MouseEvent e).
     */
    public void mouseReleased(int p_146118_1_, int p_146118_2_) {}

    /**
     * Returns true if the mouse has been pressed on this control. Equivalent of MouseListener.mousePressed(MouseEvent
     * e).
     */
    public boolean mousePressed(Minecraft p_146116_1_, int p_146116_2_, int p_146116_3_)
    {
        return this.enabled && this.visible && p_146116_2_ >= this.xPosition && p_146116_3_ >= this.yPosition && p_146116_2_ < this.xPosition + this.width && p_146116_3_ < this.yPosition + this.height;
    }
}
