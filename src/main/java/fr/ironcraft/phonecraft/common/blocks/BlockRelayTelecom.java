package fr.ironcraft.phonecraft.common.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.client.ClientProxy;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityRelayTelecom;
import fr.ironcraft.phonecraft.packet.PacketRelayTelecom;

/**
 * @authors Dermenslof, DrenBx
 */
public class BlockRelayTelecom extends BlockContainer
{

	public BlockRelayTelecom()
	{
		super(Material.iron);
		this.setHardness(1.0F).setResistance(10.0F).setStepSound(soundTypeMetal);
	}

	public boolean isOpaqueCube()
	{
		return false;
	}

	@SideOnly(Side.CLIENT)
	public boolean renderAsNormalBlock()
	{
		return false;
	}
	
	@SideOnly(Side.CLIENT)
	public int getRenderType()
	{
		return ClientProxy.renderRelayTelecomID;
	}
	
	@SideOnly(Side.CLIENT)
    public float getAmbientOcclusionLightValue(IBlockAccess par1IBlockAccess, int par2, int par3, int par4)
    {
        return 0.2F;
    }

	public void onBlockAdded(World par1World, int x, int y, int z)
	{
		super.onBlockAdded(par1World, x, y, z);
	}
	
	public void breakBlock(World par1World, int par2, int par3, int par4, Block par5, int par6)
    {
        super.breakBlock(par1World, par2, par3, par4, par5, par6);
        par1World.removeTileEntity(par2, par3, par4);
    }

	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
		this.blockIcon = par1IconRegister.registerIcon("relayTelecom");
	}
	

	@Override
	public TileEntity createNewTileEntity(World var1, int var2)
	{
		return new TileEntityRelayTelecom();
	}
}
