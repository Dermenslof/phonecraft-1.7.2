package fr.ironcraft.phonecraft.client.gui.button;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;

import org.lwjgl.opengl.GL11;

import fr.ironcraft.phonecraft.client.gui.GuiPhoneInGame;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;

public class GuiPhoneButtonLink extends GuiPhoneButton
{
	protected GuiPhoneInGame gui;
	protected ResourceLocation img;
	protected boolean hasIcon;
	protected int iconX;
	protected int iconY;
	protected boolean customTexture;
	protected int texture;
	protected int textureOffsetX;
	protected int textureOffsetY;
	protected int textureWidth;
	protected int textureHeight;
	protected float textureScale;
	public boolean isOver;
	private String link;
	
    public GuiPhoneButtonLink(int id, int x, int y, String title, String link)
    {
        this(id, x, y, 10, 7, title, link);
    }
    
    public GuiPhoneButtonLink(int id, int x, int y, int width, int height, String title, String link)
    {
        super(GuiPhoneInGame.instance, id, x, y, width, height, title);
        this.link = link;
    }

	public boolean hasIcon()
	{
		return false;
	}
	
    public void drawButton(Minecraft p_146112_1_, int p_146112_2_, int p_146112_3_)
    {
        if (this.visible)
        {
            GL11.glColor4f(1.0F, 1.0F, 1.0F, GuiPhoneInGame.instance.getTransparency());
            this.field_146123_n = p_146112_2_ >= this.xPosition && p_146112_3_ >= this.yPosition && p_146112_2_ < this.xPosition + this.width && p_146112_3_ < this.yPosition + this.height;
            int k = this.getHoverState(this.field_146123_n);
            this.isOver = this.field_146123_n;
            this.mouseDragged(p_146112_1_, p_146112_2_, p_146112_3_);
            int l = 0x0088aa;
            if (packedFGColour != 0)
                l = packedFGColour;
            else if (!this.enabled)
                l = 10526880;
            else if (this.field_146123_n)
                l = 0xff0000;
            GL11.glPushMatrix();
            GL11.glScalef(0.5F, 0.5F, 1);
            GuiPhoneInGame.instance.drawString(this.displayString, (int)(this.xPosition / 0.5F), (int)(this.yPosition / 0.5F) - 4, l);
            GL11.glPopMatrix();
        }
    }
    
    public void func_146113_a(SoundHandler p_146113_1_)
    {
        p_146113_1_.playSound(PositionedSoundRecord.func_147674_a(new ResourceLocation("gui.button.press"), 1.0F));
    }
    
    public void action()
    {
    	if (this.isOver)
    	{
    		try
    		{
    			URI u = URI.create(link);
    			Desktop.getDesktop().browse(u);
    			func_146113_a(Minecraft.getMinecraft().getSoundHandler());
    		}
    		catch(IOException e)
    		{
    			e.printStackTrace();
    		}
    	}
    }
}
