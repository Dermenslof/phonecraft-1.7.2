package fr.ironcraft.phonecraft.packet;

import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityQrCode;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityRelayTelecom;
import fr.ironcraft.phonecraft.network.ThreadUpdateNetWork;
import io.netty.channel.ChannelHandlerContext;

import java.io.IOException;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

/**
 * @author Dermenslof, DrenBx
 */
public class PacketRelayTelecom implements IBasePacket
{
	private int xCoord;
	private int yCoord;
	private int zCoord;
	private boolean isConnected;
	
	public PacketRelayTelecom()
	{
		
	}

	public PacketRelayTelecom(int x, int y, int z, boolean isConnected)
	{
		this.xCoord = x;
		this.yCoord = y;
		this.zCoord = z;
		this.isConnected = isConnected;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, PacketBuffer buffer)
	{
		buffer.writeInt(this.xCoord);
    	buffer.writeInt(this.yCoord);
    	buffer.writeInt(this.zCoord);
		buffer.writeInt((this.isConnected ? 1 : 0));
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, PacketBuffer buffer)
	{
		this.xCoord = buffer.readInt();
		this.yCoord = buffer.readInt();
		this.zCoord = buffer.readInt();
		this.isConnected = buffer.readInt() == 0 ? false : true;
	}

	@Override
	public void handleClientSide(EntityPlayer player)
	{
		World w = player.worldObj;
		TileEntity tile = w.getTileEntity(xCoord, yCoord, zCoord);
		if (tile != null)
		{
			if (tile instanceof TileEntityRelayTelecom)
				((TileEntityRelayTelecom)tile).isConnected = this.isConnected;
		}
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		World w = player.worldObj;
		TileEntity tile = w.getTileEntity(xCoord, yCoord, zCoord);
		if (tile != null)
		{
			if (tile instanceof TileEntityRelayTelecom)
				((TileEntityRelayTelecom)tile).isConnected = this.isConnected;
		}
		Phonecraft.packetPipeline1.sendToAll(this);
		new Thread(new ThreadUpdateNetWork(w, xCoord, yCoord, zCoord, isConnected)).start();
	}
}