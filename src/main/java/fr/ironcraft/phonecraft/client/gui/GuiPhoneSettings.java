package fr.ironcraft.phonecraft.client.gui;

import java.io.File;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.config.Configuration;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.client.ClientProxy;
import fr.ironcraft.phonecraft.client.gui.button.EnumPhoneButton;
import fr.ironcraft.phonecraft.common.blocks.ICBlocks;
import fr.ironcraft.phonecraft.utils.TextureUtils;

public class GuiPhoneSettings extends GuiPhoneInGame
{
	private boolean isWallpaper;
	private int numWallpaper = -1;
	private int scroll = 0;
	private File wallpaperFolder;

	public GuiPhoneSettings(Minecraft par1Minecraft)
	{
		super(par1Minecraft);
		this.setClickControl(false);
	}

	@Override
	public void initGui()
	{
		super.initGui();
		scroll = 0;
		isWallpaper = false;
	}
	
	@Override
	public void updateScreen()
	{
		super.updateScreen();
		this.wallpaperFolder = new File(Phonecraft.phoneFolder, "wallpapers");
	}

	@Override
	public void drawScreen(int par1, int par2, float par3)
	{
		super.drawScreen(par1, par2, par3);
		String title = I18n.format("gui.phone.settings", new Object[0]);
		GL11.glPushMatrix();
		GL11.glScalef(0.5F, 0.5F, 1);
		this.drawString(title, (int)((this.getScreenPosX() + this.getScreenWidth() / 2 - this.getFont().getStringWidth(title) / 4) / 0.5F), (int)((this.getScreenPosY() + 4) / 0.5F), 0xffffffff);
		GL11.glPopMatrix();
		int size = wallpaperFolder.list().length + 1;
		int verticalShift = 0;
		if (isWallpaper)
		{
			if (40 + this.scroll > 30)
			{
				this.drawRect(0, 40 + this.scroll, this.getScreenWidth(), 49 + this.scroll, 0xff353535,this.transparency);
				this.drawRect(1, 41 + this.scroll, this.getScreenWidth() - 1, 48 + this.scroll, 0xff252525,this.transparency);
				GL11.glPushMatrix();
				GL11.glScalef(0.5F, 0.5F, 1);
				this.drawString("default", (int)((this.getScreenPosX() + 5) / 0.5F), (int)((this.getScreenPosY() + 40 + this.scroll) / 0.5F), 0xffffffff);
				GL11.glPopMatrix();
				if (this.wallpaper < 0)
				{
					GL11.glPushMatrix();
					this.mc.renderEngine.bindTexture(new ResourceLocation(TextureUtils.getTextureNameForGui("icons/ok")));
					GL11.glScalef(1F / 40F, 1F / 40F, 1);
					this.drawTexturedModalRect((int)((this.getScreenPosX() + this.getScreenWidth() - (size > 3 ? 15 : 8)) / (1F / 40F)), (int)((this.getScreenPosY() + 41 + this.scroll) / (1F / 40F)), 0, 0, 256, 256);
					GL11.glPopMatrix();
				}
			}
			for (int i=1; i<wallpaperFolder.listFiles().length + 1; ++i)
			{
				if (40 + i * 8 + this.scroll > 30 && 40 + i * 8 + this.scroll <= 30 + 4 * 8)
				{
					this.drawRect(0, 40 + i * 8 + this.scroll, this.getScreenWidth(), 49 + i * 8 + this.scroll, 0xff353535,this.transparency);
					this.drawRect(1, 41 + i * 8 + this.scroll, this.getScreenWidth() - 1, 48 + i * 8 + this.scroll, 0xff252525,this.transparency);
					GL11.glPushMatrix();
					GL11.glScalef(0.5F, 0.5F, 1);
					this.drawString(wallpaperFolder.list()[i - 1], (int)((this.getScreenPosX() + 5) / 0.5F), (int)((this.getScreenPosY() + 40 + i * 8 + this.scroll) / 0.5F), 0xffffffff);
					GL11.glPopMatrix();
				}
				if (wallpaperName.equals(wallpaperFolder.list()[i - 1]))
				{
					GL11.glPushMatrix();
					this.mc.renderEngine.bindTexture(new ResourceLocation(TextureUtils.getTextureNameForGui("icons/ok")));
					GL11.glScalef(1F / 40F, 1F / 40F, 1);
					this.drawTexturedModalRect((int)((this.getScreenPosX() + this.getScreenWidth() - (size > 3 ? 15 : 8)) / (1F / 40F)), (int)((this.getScreenPosY() + 41 + i * 8 + this.scroll) / (1F / 40F)), 0, 0, 256, 256);
					GL11.glPopMatrix();
				}
			}
			if (size > 3)
			{
				this.drawRect(this.getScreenWidth() - 8, 40, this.getScreenWidth(), 40 + 3*8, 0xff353535, 0);
				this.drawRect(this.getScreenWidth() - 7, 41, this.getScreenWidth() - 1, 39 + 3*8, 0xff252525, 0);
				int maxScroll = size > 3 ? size * 8 - 8 * 3 : 1;
				int percent = ((-this.scroll * 100) / maxScroll);
				int dec = ((3 * 8 - 8) * percent) / 100;
				this.drawRect(this.getScreenWidth() - 7, 41 + dec, this.getScreenWidth() - 1, 39 + dec + 8, 0xff757575, 0);
			}
		}
		String[] titles = {I18n.format("gui.phone.wallpaper", new Object[0]), "test", "test"};
		for (int i=0; i<titles.length; ++i)
		{
			if (isWallpaper && i > 0)
				verticalShift = 3 * 8;
			GL11.glPushMatrix();
			this.drawRect(0, 20 + (i * 20) + verticalShift, this.getScreenWidth(), 40 + (i * 20) + verticalShift, 0xff353535, 0);
			this.drawRect(1, 21 + (i * 20) + verticalShift, this.getScreenWidth() - 1, 39 + (i * 20) + verticalShift, 0xff252525,0);
			this.drawGradientRect(1, 21 + (i * 20) + verticalShift, this.getScreenWidth() - 1, 39 + (i * 20) + verticalShift, 0x55252525, 0x55ffffff);
			this.drawRoundedRect(this.getScreenWidth() - 15, 25 + (i * 20) + verticalShift, this.getScreenWidth() - 5, 35 + (i * 20) + verticalShift, 2, 0x55252525, 0F);
			GL11.glPushMatrix();
			GL11.glScalef(0.5F, 0.5F, 1);
			this.drawString(titles[i], (int)((this.getScreenPosX() + 5) / 0.5F), (int)((this.getScreenPosY() + 25 + (i * 20) + verticalShift) / 0.5F), 0xffffffff);
			this.drawString("v", (int)((this.getScreenPosX() + this.getScreenWidth() - 12) / 0.5F), (int)((this.getScreenPosY() + 25 + (i * 20) + verticalShift) / 0.5F), 0xffffffff);
			GL11.glPopMatrix();
			GL11.glPopMatrix();
		}
		int x = Mouse.getEventX() * this.width / this.mc.displayWidth;
		int y = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;
		onMouseOverPhone(x, y);
	}

	@Override
	public void handleMouseInput()
	{
		super.handleMouseInput();
		int x = Mouse.getEventX() * this.width / this.mc.displayWidth;
		int y = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;
		int w = Mouse.getDWheel();
		int size = new File(Phonecraft.phoneFolder, "wallpapers").list().length + 1;
		int maxScroll = size > 3 ? size * 8 - 8 * 3: 0;
		if (w != 0 &&  size > 2)
		{
			if (w > 0)
				this.scroll += 8;
			else if (w < 0)
				this.scroll -= 8;
			if (this.scroll < -maxScroll)
				this.scroll = -maxScroll;
			else if (this.scroll > 0)
				this.scroll = 0;
		}
	}

	@Override
	public void mouseClicked(int i, int j, int k)
	{
		super.mouseClicked(i, j, k);
		if (this.getFocus())
		{
			switch (this.buttonType)
			{
			case BUTTON:
				switch(this.bouton)
				{
				case 1:
					if (this.numWallpaper > -1)
					{
						GuiPhoneInGame.wallpaper = ClientProxy.imageLoader.setupWallpapers("wallpapers:" + wallpaperFolder.listFiles()[this.numWallpaper].getName());
						this.wallpaperName= wallpaperFolder.listFiles()[this.numWallpaper].getName();
					}
					else
					{
						GuiPhoneInGame.wallpaper = -1;
						this.wallpaperName= "default";
					}
					Configuration config = Phonecraft.config;
					config.load();
					config.get(Configuration.CATEGORY_GENERAL, "wallpaper", -1).set(this.numWallpaper);
					if (config.hasChanged())
						config.save();
					return;
				case 2:
					this.isWallpaper = !this.isWallpaper;
					this.scroll = 0;
					return;
				}
			default:
				;
			}
		}
	}

	protected void onMouseOverPhone(int x, int y)
	{
		super.onMouseOverPhone(x, y);
		if (!this.getFocus() || this.buttonType != EnumPhoneButton.NULL)
			return;
		int size = wallpaperFolder.list().length + 1;
		if (x >= this.getScreenPosX() && x <= this.getScreenPosX() + this.getScreenWidth()  + (isWallpaper && size > 3 ? -8 : 0))
		{
			if (this.isWallpaper)
			{
				if (y >= this.getScreenPosY() + 42 && y <= this.getScreenPosY() + 40 + 3 * 8)
				{
					if (y >= this.getScreenPosY() + 39 + this.scroll && y <= this.getScreenPosY() + 49 + this.scroll)
					{
						this.drawRect(1, 41 + this.scroll, this.getScreenWidth() - (size > 3 ? 8 : 0) - 1, 48 + this.scroll, 0x22ffffff, -0.6F);
						this.bouton = 1;
						this.buttonType = EnumPhoneButton.BUTTON;
						this.numWallpaper = -1;
						return;
					}
					for (int i=1; i<wallpaperFolder.listFiles().length + 1; ++i)
					{
						if (y >= this.getScreenPosY() + 39 + i * 8 + this.scroll && y <= this.getScreenPosY() + 49 + i * 8 + this.scroll)
						{
							this.drawRect(1, 41 + i * 8 + this.scroll, this.getScreenWidth() - (size > 3 ? 8 : 0) - 1, 48 + i * 8 + this.scroll, 0x22ffffff, -0.6F);
							this.bouton  = 1;
							this.buttonType = EnumPhoneButton.BUTTON;
							this.numWallpaper = i - 1;
							return;
						}
					}
				}
			}
			if (x >=  this.getScreenPosX() + this.getScreenWidth() - 15 && x <= this.getScreenPosX() + this.getScreenWidth() - 5)
			{
				if (y >= this.getScreenPosY() + 25 && y <= this.getScreenPosY() + 35)
				{
					this.drawRoundedRect(this.getScreenWidth() - 15, 25, this.getScreenWidth() - 5, 35, 2, 0x22ffffff, -0.6F);
					this.bouton = 2;
					this.buttonType = EnumPhoneButton.BUTTON;
					return;
				}
			}
		}
	}
}
