package fr.ironcraft.phonecraft.network;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneInGame;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityAntena;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityRelayTelecom;

/**
 * @authors Dermenslof, DrenBx
 */
public class ThreadNetWork implements Runnable
{
	private GuiPhoneInGame gui;
	
	public ThreadNetWork(GuiPhoneInGame gui)
	{
		this.gui = gui;
	}
	
	public void run()
	{
		long now;
		int dist;
		while (this.gui.isActive && this.gui.mc.thePlayer != null)
		{
			int lastDist = 51;
			dist = 51;
			now = System.currentTimeMillis();
			if (this.gui.last != 0 && now - this.gui.last < 2000)
				continue;
			this.gui.last = now;
			if (this.gui.mc.thePlayer == null)
				return;
			World w = this.gui.mc.theWorld;
			int i = (int)this.gui.mc.thePlayer.posX;
			int j = (int)	this.gui.mc.thePlayer.posY;
			int k = (int)this.gui.mc.thePlayer.posZ;
			TileEntity te = null;
			for (int x=-200; x < 200; x++)
			{
				for (int y=-200; y < 200; y++)
				{
					for (int z=-200; z < 200; z++)
					{
						te = w.getTileEntity(i + x, j+ y, k + z);
						if (te instanceof TileEntityRelayTelecom || te instanceof TileEntityAntena)
						{
							int a = x == 0 ? 1 : x;
							int b = y == 0 ? 1 : y;
							int c = z == 0 ? 1 : z;
							double t = Math.sqrt(a * a + b * b);
							if (te instanceof TileEntityRelayTelecom)
							{
								TileEntityRelayTelecom tert = (TileEntityRelayTelecom)te;
								if (!tert.isConnected)
									continue;
							}
							dist = (int)this.gui.mc.thePlayer.getDistance(i + x, j + y, k + z) / 4;
							if (dist < lastDist)
								lastDist = dist;
						}
					}
				}	
			}
			this.gui.netWork = lastDist;
			URL url;
			URLConnection conn;
			try
			{
				url = new URL("http://www.google.com");
				conn = url.openConnection();
				conn.connect();
				this.gui.has3g = true;
			}
			catch (MalformedURLException e){}
			catch (IOException e)
			{
				this.gui.has3g = false;
			}
		}
		this.gui.isActive = true;
	}
}
