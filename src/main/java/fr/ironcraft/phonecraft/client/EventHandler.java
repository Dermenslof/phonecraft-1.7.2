package fr.ironcraft.phonecraft.client;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;
import net.minecraftforge.event.world.BlockEvent;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneCamera;
import fr.ironcraft.phonecraft.client.gui.GuiQrCodeEdit;
import fr.ironcraft.phonecraft.common.blocks.BlockQrCode;
import fr.ironcraft.phonecraft.common.blocks.ICBlocks;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityQrCode;
import fr.ironcraft.phonecraft.packet.PacketRelayTelecom;
import fr.ironcraft.phonecraft.utils.http.TryOpenGui;

/**
 * @author Dermenslof, DrenBx
 */
public class EventHandler
{
	private static boolean isOp = false;
	
	@SubscribeEvent
	public void onRender(RenderGameOverlayEvent event)
	{
		if (!(FMLClientHandler.instance().getClient().currentScreen instanceof GuiPhoneCamera))
			return;
		if (!event.type.equals(ElementType.ALL) && !event.type.equals(ElementType.TEXT))
			event.setCanceled(true);
	}

	@SubscribeEvent
	public void onPlayerJoinWorld(EntityJoinWorldEvent event)
	{
		Minecraft mc = FMLClientHandler.instance().getClient();
		Entity e = event.entity;
		if (e instanceof EntityClientPlayerMP)
		{
			EntityClientPlayerMP player = (EntityClientPlayerMP)e;
			if (player == mc.thePlayer)
				Minecraft.getMinecraft().refreshResources();
		}
	}
	
	@SubscribeEvent
	public void onPlayerInteract(PlayerInteractEvent event)
	{
		Minecraft mc = FMLClientHandler.instance().getClient();
		EntityPlayer e = event.entityPlayer;
		if (event.action != Action.RIGHT_CLICK_BLOCK)
			return;
		ItemStack i = event.entityPlayer.getCurrentEquippedItem();
		if (i != null && i.getItem() == new ItemStack(ICBlocks.relayTelecom).getItem())
		{
				Phonecraft.packetPipeline1.sendToServer(new PacketRelayTelecom(event.x, event.y, event.z, false));
				return;
		}
		if (i != null && i.getItem() == new ItemStack(ICBlocks.antena).getItem())
		{
				Phonecraft.packetPipeline1.sendToServer(new PacketRelayTelecom(event.x, event.y, event.z, true));
				return;
		}
		Block b = mc.theWorld.getBlock(event.x, event.y, event.z);
		if (b == null || b != ICBlocks.qrCode)
			return;
		TileEntityQrCode tile = (TileEntityQrCode)mc.theWorld.getTileEntity(event.x,event.y, event.z);
		if (tile == null)
			return;
		if (mc.isSingleplayer())
			mc.displayGuiScreen(new GuiQrCodeEdit(mc, tile));
		else
			new Thread(new TryOpenGui(tile, e.getDisplayName())).start();
	}
	
	@SubscribeEvent
	public void onPlayerAddBlock(BlockEvent.BreakEvent event)
	{
		Block b = event.block;
		if (b == null || (b != ICBlocks.relayTelecom && b != ICBlocks.antena))
			return;
		Phonecraft.packetPipeline1.sendToServer(new PacketRelayTelecom(event.x, event.y, event.z, false));
	}
}
