package fr.ironcraft.phonecraft.common.items;

import fr.ironcraft.phonecraft.client.render.RenderTileEntityRelayTelecom;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityRelayTelecom;
import fr.ironcraft.phonecraft.model.ModelRelayTelecom;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;

/**
 * @authors Dermenslof, DrenBx
 */
public class RenderItemRelayTelecom implements IItemRenderer
{

	private ModelRelayTelecom model;

	public RenderItemRelayTelecom()
	{
		model = new ModelRelayTelecom();
	}

	public boolean handleRenderType(ItemStack itemstack, ItemRenderType type)
	{
		return true;
	}

	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack itemstack, ItemRendererHelper helper)
	{
		return true;
	}

	public void renderItem(ItemRenderType type, ItemStack item, Object... data)
	{
		RenderTileEntityRelayTelecom.instance.renderTileEntityAt(new TileEntityRelayTelecom(), 0.0D, 0.0D, 0.0D, 0.0F);
	}
}
