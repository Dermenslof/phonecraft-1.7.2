package fr.ironcraft.phonecraft.utils.sound;

import fr.ironcraft.phonecraft.Phonecraft;

/**
 * @authors Dermenslof, DrenBx
 */
public class SoundUtils
{
	public static String getSoundPath(String name)
	{
		return Phonecraft.MODID + ":" + name;
	}
}
