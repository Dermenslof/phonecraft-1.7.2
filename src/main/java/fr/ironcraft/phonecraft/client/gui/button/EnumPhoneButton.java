package fr.ironcraft.phonecraft.client.gui.button;

/**
 * @authors Dermenslof, DrenBx
 */
public enum EnumPhoneButton
{
	NULL, MAIN, RETURN, BUTTON, APP, TASKBAR
}
