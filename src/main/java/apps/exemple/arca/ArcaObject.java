package apps.exemple.arca;

import fr.ironcraft.phonecraft.client.gui.GuiPhoneInGame;

public class ArcaObject
{
	protected int id;
	protected float posX;
	protected float posY;
	protected int width;
	protected int height;
	protected int color;
	
	protected GuiPhoneInGame gui;
	
	protected boolean enabled;
	
	public ArcaObject(int id, GuiPhoneInGame gui, int width, int height)
	{
		this(id, gui, 0, 0, width, height, 0xffffff);
	}
	
	public ArcaObject(int id, GuiPhoneInGame gui, int posX, int posY, int width, int height, int color)
	{
		this.id = id;
		this.gui = gui;
		this.posX = posX;
		this.posY = posY;
		this.width = width;
		this.height = height;
		this.color = color;
		this.enabled = true;
	}
	
	protected void drawObject()
	{
		if (this.enabled)
			gui.drawRect((int)posX, (int)posY, (int)posX + width, (int)posY + height, 0xffffff, 0);
	}
}
