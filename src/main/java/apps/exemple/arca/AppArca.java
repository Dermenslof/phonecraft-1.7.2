package apps.exemple.arca;

import fr.ironcraft.phonecraft.api.PhoneApps;
import fr.ironcraft.phonecraft.client.ClientProxy;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneInGame;
import fr.ironcraft.phonecraft.utils.TextureUtils;

public class AppArca implements PhoneApps
{
	public int icon;
	
	public void init()
	{
		this.icon = ClientProxy.imageLoader.setupTexture(TextureUtils.getTextureNameForApp(this, this.getIconName()));
	}
	
	@Override
	public String appname()
	{
		return "Arca";
	}

	@Override
	public String version()
	{
		return "1.0";
	}

	@Override
	public GuiPhoneInGame ScreenInstance()
	{
		return new GuiPhoneArcaMenu();
	}

	@Override
	public String getIconName()
	{
		return "favicon.png";
	}
	
	public int getIcon()
	{
		return this.icon;
	}
}
