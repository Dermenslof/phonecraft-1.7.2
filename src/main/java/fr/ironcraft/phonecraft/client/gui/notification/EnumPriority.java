package fr.ironcraft.phonecraft.client.gui.notification;

public enum EnumPriority
{
	NORMAL, LOW, HIGH, CRITICAL
}