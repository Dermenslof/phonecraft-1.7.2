package fr.ironcraft.phonecraft.client.gui;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.resources.I18n;

import org.jcodec.api.FrameGrab;
import org.jcodec.api.JCodecException;
import org.jcodec.containers.mp4.MP4Util;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.client.AppRegistry;
import fr.ironcraft.phonecraft.client.ClientProxy;
import fr.ironcraft.phonecraft.client.gui.button.EnumPhoneButton;
import fr.ironcraft.phonecraft.client.gui.button.GuiPhoneButton;
import fr.ironcraft.phonecraft.client.gui.button.GuiPhoneButtonMenu;
import fr.ironcraft.phonecraft.client.gui.button.GuiPhoneButtonMovie;
import fr.ironcraft.phonecraft.utils.ImageLoader;
import fr.ironcraft.phonecraft.utils.camera.MovieLoader;
import fr.ironcraft.phonecraft.utils.camera.MoviePlayer;

/**
 * @author Dermenslof, DrenBx
 */
@SideOnly(Side.CLIENT)
public class GuiPhoneVideo extends GuiPhoneInGame
{
	protected int prevVideo;
	protected List<String> videos;
	protected BufferedImage buffered;
	protected boolean menuVideo;
	protected ImageLoader imgLoader;
	protected int video;
	protected int texture;
	protected boolean rotate;
	private boolean isPlaying;
	private boolean init;

	private long test;
	private long lastFrameTime;
	private int frame = -1;
	private int scroll;
	private boolean canClick;

	private List<MoviePlayer> players = new ArrayList<MoviePlayer>();

	public GuiPhoneVideo(Minecraft par1Minecraft)
	{
		super(par1Minecraft);
		this.imgLoader = new ImageLoader();
	}
	
	@Override

	public void initGui()
	{
		this.buttonList.clear();
		super.initGui();

		//buttons;
		int[] ic = {64, 80, 66, 65};
		for (int t=0; t < 4; ++t)
		{
			GuiPhoneButton b = new GuiPhoneButtonMenu(this, t + 2, 1 + (t * 23), this.getScreenHeight() - 19, 23, 18, "", true);
			b.setIcon(this.getTextureIcons(), 3, 1, ic[t], 1F);
			this.buttonList.add(b);
		}
		this.setFocus(true, false);
		this.videos = getVideosAvaiable();
		this.screen = 4;
		this.menuVideo = true;
		this.video = -1;
		this.prevVideo = -1;
		this.test = 0;
		this.players.clear();
		init = false;
	}

	public boolean doesGuiPauseGame()
	{
		return false;
	}

	@Override
	public void updateScreen()
	{
		super.updateScreen();
		if (!this.init)
		{
			this.init = true;
			MoviePlayer mp;
			int i = 0;
			int size = this.videos.size();
			int spaceX = size > 12 ? 10 : 20;
			int spaceY = size > 10 ? 5 : 10;
			for (String s: this.videos)
			{
				File file = null;
				file = new File(this.mc.mcDataDir, Phonecraft.phoneFolder + "media/movies/" + s);
				if (file.exists())
				{
					mp = new MoviePlayer(file);
					this.players.add(mp);
					this.buttonList.add(new GuiPhoneButtonMovie(this, 6 + i, 12 + (i % 2) * spaceX + (i % 2) * 25 - 1, 0, mp));
				}
				i++;
			}
		}
//		if (this.video != this.prevVideo && this.video >= 0)
//		{
//			ClientProxy.movie.clear();
//			this.isPlaying = false;
//			this.test = 0;
//			int tmp = this.video;
//			this.video = -1;
//			this.prevVideo = -1;
//			this.videos = getVideosAvaiable();
//			if (this.videos.size() > 0)
//			{
//				File file = null;
//				try
//				{
//					String name = this.videos.get(tmp);
//					file = new File(this.mc.mcDataDir, Phonecraft.phoneFolder + "media/movies/" + name);
//				}
//				catch(Exception e)
//				{
//					;
//				}
//				//				if (file.exists())
//				//				{
//				//					if (this.thread != null && this.thread.isAlive())
//				//						this.thread.interrupt();
//				//					this.thread = new Thread(new MovieLoader(file, this.imgLoader));
//				//					this.thread.start();
//				this.prevVideo = this.video = tmp;
//				this.frame = -1;
//				//				}
//			}
//		}

	}

	@Override
	protected void actionPerformed(GuiButton button)
	{
		super.actionPerformed(button);
		if (button.id > 5 && this.video < 0)
		{
			this.canClick = false;
			this.video = button.id - 6;
			this.players.get(this.video).load();
			this.test = -1;
			this.frame = -1;
			this.isPlaying = false;
			return;
		}
		switch (button.id)
		{
		case 2:
			if(this.video > -1)
				this.mc.displayGuiScreen(new GuiPhoneEditImg(this.mc, new File(this.videos.get(this.video))));
			break;
		case 3:
			this.video = -1;
			this.frame = 0;
			this.isPlaying = false;
			break;
		case 4:
			this.rotate = !this.rotate;
			break;
		case 5:
			try
			{
				String name = this.videos.get(this.video);
				File f = new File(this.mc.mcDataDir, Phonecraft.phoneFolder + "media/movies/" + name);
				f.delete();
				this.initGui();
			}
			catch(Exception e)
			{
				this.video = -1;	
			}
			break;
		}
	}

	public void mouseClicked(int i, int j, int k)
	{
		super.mouseClicked(i, j, k);
		if (this.getFocus())
		{
			if(k == 1 && this.video > -1 && i >= getScreenPosX() && i <= getScreenPosX() + getScreenWidth())
			{
				if(j >= getScreenPosY() && j <= getScreenPosY() + getScreenHeight() - 20)
				{
					this.menuVideo = !this.menuVideo;
					boolean hideButtons = isHome && !isAnimated;
					for (Object b: this.buttonList)
					{
						if (((GuiButton)b).id >= 2 && ((GuiPhoneButton)b).id <= 5)
							((GuiButton)b).visible = !((GuiButton)b).visible;
					}
				}
			}
		}
	}

	@Override
	protected void mouseMovedOrUp(int x, int y, int which)
	{
		super.mouseMovedOrUp(x, y, which);
		if (which != 0)
			return;
		if (getFocus())
		{
			boolean flip = false;
			if (!this.rotate && x >= getScreenPosX() && x <= getScreenPosX() + getScreenWidth())
			{
				if(y >= getScreenPosY() + 39 && y <= getScreenPosY() + 95)
					flip = true;
			}
			else if (this.rotate && !this.menuVideo && isInScreen())
				flip = true;
			else if (this.rotate && this.menuVideo)
			{
				if (x >= getScreenPosX() + 12 && x <= getScreenPosX() + getScreenWidth())
				{
					if(y >= getScreenPosY() + 8 && y <= getScreenPosY() + getScreenHeight() - 20)
						flip = true;
				}
			}
			if (this.video > -1 && flip && this.canClick)
				this.isPlaying = !this.isPlaying;
		}
		this.canClick = true;
	}

	public void drawScreen(int par1, int par2, float par3)
	{
		super.drawScreen(par1, par2, par3);
		this.drawRect(0, 0, getScreenWidth(), getScreenHeight(), 0xff000000, 0);
		if (this.video < 0)
		{
			this.rotate = false;
			if (this.players.size() > 0)
				drawList(par1, par2, par3);
			else
			{
				String str = I18n.format("gui.phone.empty", new Object[0]);
				this.drawString(this.fontRendererObj, str, getScreenPosX() + getScreenWidth() / 2 - this.fontRendererObj.getStringWidth(str) / 2, getScreenPosY() + getScreenHeight() / 2 - 15, 0xffffff);
			}	
		}
		else
			drawVideo(par1, par2, par3);
		//buttons background
		if (this.menuVideo && this.video > -1)
		{
			GL11.glPushMatrix();
			GL11.glColor4f(1, 1, 1, this.transparency);
			this.drawGradientRect(0, this.getScreenHeight() - 20, this.getScreenWidth(), this.getScreenHeight(), 0x323232, 0x111111, 0F);
			GL11.glPopMatrix();
			for (Object b: this.buttonList)
			{
				if (!(b instanceof GuiPhoneButtonMovie))
					((GuiButton)b).drawButton(mc, par1, par2);
			}
		}
		onMouseOverPhone(par1, par2);
	}

	public void drawList(int par1, int par2, float par3)
	{
		int size = this.videos.size();
		int spaceY = size > 10 ? 22 : 27;
		int i = 0;
		for (Object b: this.buttonList)
		{
			if (b instanceof GuiPhoneButtonMovie)
			{
				GuiPhoneButton gb = ((GuiPhoneButton)b);
				gb.visible = false;
				gb.setPosition(gb.xPosition,  10 + i / 2 * spaceY + getScreenPosY() + this.scroll);
				if (gb.yPosition > getScreenPosY() && gb.yPosition + gb.height < getScreenPosY() + getScreenHeight())
					gb.visible = true;
				gb.drawButton(mc, par1, par2);
				i++;
			}
		}
		if (size > 12)
		{
			int maxScroll = size > 12 ? (size / 2) * spaceY - (6 * spaceY) + (size % 2 == 0 ? 0 : spaceY): 0;
			int percent = ((-this.scroll * 100) / maxScroll);
			int dec = ((getScreenHeight() - 4 - 8) * percent) / 100;
			int height = (getScreenHeight() - 2) / (maxScroll / spaceY);
			this.drawRect(getScreenWidth() - 12, 1, getScreenWidth() - 1, getScreenHeight() - 1, 0x757575, 1F);
			this.drawRect(getScreenWidth() - 11, 2, getScreenWidth() - 2, getScreenHeight() - 2, 0x252525, 1F);
			this.drawRect(getScreenWidth() - 11, 2 + dec, getScreenWidth() - 11 + 9, 2 + 8 + dec, 0x555555, 1F);
			this.drawRect(getScreenWidth() - 11 + 1, 2 + 1 + dec, getScreenWidth() - 11 + 9 - 1, 2 + 8 - 1 + dec, 0x454545, 1F);
			this.drawRect(getScreenWidth() - 11 + 3, 2 + 4 + dec, getScreenWidth() - 11 + 9 - 3, 3 + 4 + dec, 0, 1F);
		}
	}

	public void drawVideo(int par1, int par2, float par3)
	{
		MoviePlayer mp = this.players.get(this.video);
		if (this.rotate && this.menuVideo)
		{
			this.drawRect(11, 0, getScreenWidth(), getScreenHeight(), 0x828282, 0F);
			this.drawRect(12, 1, getScreenWidth() - 1, getScreenHeight() - 1, 0x121212, 0F);
		}
		else if (!this.rotate)
		{
			this.drawRect(0, 39 + (this.menuVideo ? 0 : 5), getScreenWidth(), 95 + (this.menuVideo ? 0 : 5), 0x828282, 0F);
			this.drawRect(1, 40 + (this.menuVideo ? 0 : 5), getScreenWidth() - 1, 94 + (this.menuVideo ? 0 : 5), 0x121212, 0F);
		}
		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
		if (this.video >= 0 && mp.getLength() > 0 && !mp.isLoading() && this.frame > 0)
		{
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.frame);
			if(this.rotate)
			{
				GL11.glTranslatef(this.getScreenPosX() + this.getScreenWidth(), this.getScreenPosY(), 0);
				GL11.glRotatef(90F, 0, 0, 1);
				GL11.glScalef(0.579F, 0.3600256F, 1);
			}
			else
			{
				GL11.glTranslatef(this.getScreenPosX() + 0.7F, this.getScreenPosY() + 40F + (this.menuVideo ? 0 : 5), 0);
				GL11.glScalef(0.354F, 0.21230769F, 1);
			}
			GL11.glColor4f(1,  1,  1,  this.transparency);
			GL11.glPushMatrix();
			this.drawTexturedModalRect(0, 0, 0, 0, 256, 256);
			GL11.glPopMatrix();
		}
		GL11.glPopMatrix();
		if ((this.isPlaying || test == 0) && this.video >= 0 && !mp.isLoading())
		{
			long now = System.currentTimeMillis();
			if (now - this.lastFrameTime >= 100 && mp.getLength() > 0) // take frames at 10 fps
			{
				this.lastFrameTime = now;
				if (test < mp.getLength() - 1)
					test++;
				else
				{
					test = 0;
					this.isPlaying = false;
				}
				if (this.frame > -1)
					GL11.glDeleteTextures(this.frame);
				this.frame = this.imgLoader.setupTexture(mp.getFrame((int)test));
			}
		}
		if (this.rotate && this.menuVideo)
		{
			this.drawRect(0, 7, 11, getScreenHeight() - 20, 0x252525, 0F);
			this.drawRect(5, 17, 6, 125, 0x757575, 0F);
			if (mp.getLength() > 0)
			{
				int pos = (int) ((test * 108) / mp.getLength());
				this.drawRect(5,  17, 6, 17 + pos, 0xaa0000, 0F);
				this.drawRect(1,  17 - 2 + pos, 9, 17 + 2 + pos, 0xdedede, 0F);

			}
			GL11.glPushMatrix();
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			this.mc.renderEngine.bindTexture(this.getTextureIcons_white());
			GL11.glTranslatef(getScreenPosX() + 9, getScreenPosY() + 7, 0);
			GL11.glRotatef(90F, 0, 0, 1);
			GL11.glScalef(0.5F, 0.5F, 1);
			if (this.isPlaying)
				this.drawTexturedModalRect(0, 0, 3 * 16, 32, 16, 16);
			else
				this.drawTexturedModalRect(0, 0, 1 * 16, 32, 16, 16);
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glPopMatrix();
		}
		else if (!this.rotate)
		{
			this.drawRect(0,  getScreenHeight() - 53 + (this.menuVideo ? 0 : 5), getScreenWidth(), getScreenHeight() - 42 + (this.menuVideo ? 0 : 5), 0x252525, 0F);
			this.drawRect(10,  getScreenHeight() - 48 + (this.menuVideo ? 0 : 5), 90, getScreenHeight() - 47 + (this.menuVideo ? 0 : 5), 0x757575, 0F);
			if (mp.getLength() > 0)
			{
				int pos = (int) ((test * 80) / mp.getLength());
				this.drawRect(10,  getScreenHeight() - 48 + (this.menuVideo ? 0 : 5), 10 + pos, getScreenHeight() - 47 + (this.menuVideo ? 0 : 5), 0xaa0000, 0F);
				this.drawRect(10 - 2 + pos,  getScreenHeight() - 51 + (this.menuVideo ? 0 : 5), 10 + 2 + pos, getScreenHeight() - 44 + (this.menuVideo ? 0 : 5), 0xdedede, 0F);
			}
			GL11.glPushMatrix();
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			this.mc.renderEngine.bindTexture(this.getTextureIcons_white());
			GL11.glTranslatef(getScreenPosX(), getScreenPosY() + 97 + (this.menuVideo ? 0 : 5), 0);
			GL11.glScalef(0.5F, 0.5F, 1);
			if (this.isPlaying)
				this.drawTexturedModalRect(0, 0, 3 * 16, 32, 16, 16);
			else
				this.drawTexturedModalRect(0, 0, 1 * 16, 32, 16, 16);
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glPopMatrix();
		}
		GL11.glPushMatrix();
		GL11.glColor4f(1, 1, 1, this.transparency);
		if (mp.isLoading())
		{
			String loading = I18n.format("gui.phone.loading", new Object[0]);
			GL11.glPushMatrix();
			GL11.glTranslatef(getScreenPosX() + getScreenWidth() / 2 - (this.fontRendererObj.getStringWidth(loading)) / 4, getScreenPosY() + 62 + (this.menuVideo ? 0 : 5), 0);
			if (this.rotate)
			{
				GL11.glTranslatef(20 + (this.fontRendererObj.getStringWidth(loading)) / 4, getScreenHeight() / 2 - this.fontRendererObj.getStringWidth(loading) / 4 - 72 + (this.menuVideo ? 0 : 5), 0);
				GL11.glRotatef(90, 0, 0, 1);
			}
			GL11.glScalef(0.5F, 0.5F, 1F);
			this.drawString(this.fontRendererObj, loading, 0, 0, 0xffd2d2d2);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			this.mc.renderEngine.bindTexture(this.getTextureIcons_white());
			GL11.glTranslatef(getScreenPosX() + getScreenWidth() / 2 + 1, getScreenPosY() + 56 + 8 + 10 + (this.menuVideo ? 0 : 5), 0);
			if (this.rotate)
				GL11.glTranslatef(0, (this.menuVideo ? 0 : 5) - 10, 0);
			GL11.glRotatef((System.currentTimeMillis() / 10) % 60 * 6, 0, 0, 1);
			GL11.glTranslatef(-8, -8, 0);
			this.drawTexturedModalRect(0, 0, 66 % 16 * 16, 64, 16, 16);
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glPopMatrix();
		}
		GL11.glPopMatrix();
		if(this.menuVideo)
		{
			//barre head
			if(this.videos.size() != 0)
			{
				GL11.glPushMatrix();
				drawRect(0, 0, 92, 7, 0xff222222, 1.0F);
				GL11.glScalef(0.5F, 0.5F, 1);
				String str = String.valueOf(this.video+1)+"/"+String.valueOf(this.videos.size());
				this.drawString(str, (int) ((this.getScreenPosX() + this.getScreenWidth() / 2 - this.getFont().getStringWidth(str) / 4)/0.5F), (int)((this.getScreenPosY() - 1) / 0.5F), 0xffd2d2d2, this.transparency);
				GL11.glPopMatrix();
			}
		}
	}

	public List<String> getVideosAvaiable()
	{
		List<String> list = new ArrayList<String>();
		File dir = new File(this.mc.mcDataDir, Phonecraft.phoneFolder + "media/movies/");
		if (!dir.exists())
			dir.mkdirs();
		for(File f : dir.listFiles())
		{
			if(f.getPath().contains(".mp4") && !f.getName().contains("test"))
				list.add(f.getName());
		}
		return list;
	}
	
	@Override
	public void handleMouseInput()
	{
		super.handleMouseInput();
		if (isInScreen())
		{
			int w = Mouse.getDWheel();
			int size = this.videos.size();
			int spaceY = size > 10 ? 22 : 27;
			int maxScroll = size > 12 ? (size / 2) * spaceY - (6 * spaceY) + (size % 2 == 0 ? 0 : spaceY): 0;
			if (w != 0 &&  size > 12)
			{
				if (w > 0)
					this.scroll += spaceY;
				else if (w < 0)
					this.scroll -= spaceY;
				if (this.scroll < -maxScroll)
					this.scroll = -maxScroll;
				else if (this.scroll > 0)
					this.scroll = 0;
			}
		}
	}

	protected void onMouseOverPhone(int x, int y)
	{
		super.onMouseOverPhone(x, y);
		if(this.isTactile && !this.mouseIsDrag && this.clickX >= 0  && this.video > -1)
		{
			boolean change = false;
			if(-this.clickX + this.releaseX > 40)
			{
				change = true;
				this.video = this.video > 0 ? this.video - 1 : this.videos.size()-1;
			}
			else if(-this.clickX + this.releaseX < -40)
			{
				change = true;
				this.video = this.video < this.videos.size() - 1 ? this.video + 1 : 0;
			}
			this.isTactile = false;
			this.clickX = this.releaseX = this.releaseY = this.clickY = -1;
			if (change)
			{
				this.players.get(this.video).load();
				this.test = -1;
				this.frame = -1;
				this.isPlaying = false;
			}
		}
	}
}
