package fr.ironcraft.phonecraft.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.Language;
import net.minecraft.client.settings.GameSettings;

import org.apache.commons.io.IOUtils;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.ModClassLoader;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.api.PhoneApps;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneInGame;
import fr.ironcraft.phonecraft.client.gui.notification.EnumPriority;
import fr.ironcraft.phonecraft.client.gui.notification.EnumType;
import fr.ironcraft.phonecraft.client.gui.notification.Notification;

/**
 * @authors Dermenslof, DrenBx
 */
class JarFilter implements FilenameFilter
{
	public boolean accept(File dir, String name)
	{
		return (name.endsWith(".zip") || name.endsWith(".jar"));
	}
}

/**
 * @authors Dermenslof, DrenBx
 */
public class AppServiceLoader
{
	private List<PhoneApps> appCollection;

	private ModClassLoader ucl;

	public AppServiceLoader()
	{
		appCollection = new ArrayList<PhoneApps>();
	}
	
	public void search(File dir) throws Exception
	{
		if (dir.isFile())
			return;
		else if(!dir.exists())
		{
			System.out.println("[PhoneCraft] create apps folder");
			dir.mkdirs();
		}
		File[] files = dir.listFiles(new JarFilter());
		for (File f : files)
			findClassesInJar(PhoneApps.class, f);
	}
	
	public boolean findClassesInJar(final Class<?> baseInterface, final File jarFullPath) throws Exception
	{
		final List<String> classesTobeReturned = new ArrayList<String>();
		int init = 0;
		String path = "";
		if (!jarFullPath.isDirectory())
		{
			JarFile jar = new JarFile(jarFullPath);
			String jarName = jar.getName().replace(".zip", "").replace(Phonecraft.phoneFolder + "apps/", "");
			String zipname = jarFullPath.getName().replace(".zip", "");
			File resourcesFile = new File(jarFullPath.getParentFile(), "resources/");
			
			for (Enumeration<JarEntry> enums = jar.entries(); enums.hasMoreElements();)
			{
				JarEntry entry = (JarEntry) enums.nextElement();
				String image = entry.getName().replace("resources/", "");
				if (entry.getName().endsWith(".png") || entry.getName().endsWith(".jpg"))
				{
					File dest = new File(resourcesFile, image);
					if (init == 0)
					{
						String[] tab =  dest.getPath().split("\\" + File.separator);
						for (int i=5; i< tab.length - 2; ++i)
							path += tab[i] + File.separator;
						init = 1;
						
						path = path.replace("PhoneCraft/apps/resources/", "");
					}
//					System.out.println("[PhoneCraft] extract image " + image + " to " + dest.getPath());
					if (!resourcesFile.exists())
						resourcesFile.mkdir();
					InputStream is = jar.getInputStream(entry);
					if(!dest.exists())
					{
						if(!dest.getParentFile().exists())
							dest.getParentFile().mkdirs();
						dest.createNewFile();
					}
					FileOutputStream fos = new FileOutputStream(dest);
					while (is.available() > 0)
						fos.write(is.read());
					fos.close();
					is.close();
				}
				else if (entry.getName().endsWith(".lang"))
				{
					String[] lang_split = entry.getName().replace(".lang", "").split("\\" + File.separator);
					String lang = lang_split[lang_split.length - 1];
					System.out.println("[PhoneCraft]: Language found for " + jarName.replace("./", "") + " app: " + lang);
					InputStream is = jar.getInputStream(entry);
					BufferedReader in = new BufferedReader(new InputStreamReader(is));
					String line = null;

					while((line = in.readLine()) != null)
					{
						if (!line.equals(""))
						{
							String[] tab = line.split("=");
							if (lang.equals("en_US"))
								LanguageRegistry.instance().addStringLocalization(tab[0], tab[1]);
							LanguageRegistry.instance().addStringLocalization(tab[0], lang, tab[1]);
						}
					}
					is.close();
					in.close();
				}
			}
			jar.close();
			String currentThreadName = Thread.currentThread().getName();
			JarInputStream jarFile = null;
			ClassLoader ucl2 = Loader.instance().getModClassLoader();
			if(ucl2 instanceof ModClassLoader)
			{
				 ucl = (ModClassLoader) ucl2;
				 ucl.addFile(jarFullPath);
			}
			jarFile = new JarInputStream(new FileInputStream(jarFullPath));
			JarEntry jarEntry;
			while (true)
			{
				jarEntry = jarFile.getNextJarEntry();
				if (jarEntry == null)
					break;
				if (jarEntry.getName().endsWith(".class"))
				{
					String classname = jarEntry.getName().replaceAll("/", "\\.");
					classname = classname.substring(0, classname.length() - 6);
					if (!classname.contains("$"))
					{
						try
						{
							final Class<?> myLoadedClass = Class.forName(classname, true, ucl);
							if (baseInterface.isAssignableFrom(myLoadedClass))
							{
								PhoneApps app = (PhoneApps) myLoadedClass.newInstance();
								Phonecraft.appResourcesPath.put(app.appname(), path);
								app.init();
								appCollection.add(app);
								GuiPhoneInGame.addNotification(new Notification(EnumPriority.NORMAL, app.appname() + " install success", EnumType.INFO));
								return true;
							}
							else
							{
								Class.forName(classname, true, ucl);
								ucl.loadClass(classname);
								myLoadedClass.newInstance();
							}
						}
						catch (final ClassNotFoundException e)
						{
							e.printStackTrace();
						}
					}
				}
			}
			jarFile.close();
		}
		return false;
	}

	public void search(String directory) throws Exception
	{
		File dir = new File(directory);
		search(dir);
	}
	
	public List<PhoneApps> getAppCollection()
	{
		return appCollection;
	}

	public void setAppCollection(List<PhoneApps> appCollection)
	{
		this.appCollection = appCollection;
	}
}