package fr.ironcraft.phonecraft.utils.camera;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jcodec.api.FrameGrab;
import org.jcodec.api.JCodecException;
import org.jcodec.common.NIOUtils;
import org.jcodec.containers.mp4.MP4Util;
import org.jcodec.containers.mp4.boxes.MovieBox;

public class MoviePlayer
{
	private File file;
	private List<BufferedImage> frames = new ArrayList<BufferedImage>();
	private Thread loader;
	private int preview = -1;
	private BufferedImage screen = null;
	private boolean isLoaded;
	
	public MoviePlayer(File file)
	{
		this.file = file;
		setScreen();
	}
	
	public void load()
	{
		if ((this.loader != null && this.loader.isAlive()) || this.isLoaded)
			return;
		this.loader = new Thread(new MovieLoader(this.file, this));
		this.loader.start();
	}
	
	public File getFile()
	{
		return this.file;
	}
	
	public List<BufferedImage> getFrames()
	{
		return this.frames;
	}
	
	public BufferedImage getFrame(int i)
	{
		if (i < 0 || i > this.frames.size() - 1)
			return null;
		return this.frames.get(i);
	}
	
	public void setFrames(List<BufferedImage> frames)
	{
		this.frames = frames;
	}
	
	public long getLength()
	{
		return this.frames.size();
	}
	
	public void setLoaded()
	{
		this.isLoaded = true;
	}
	
	public boolean isLoading()
	{
		return !this.isLoaded;
	}
	
	public void setPreview(int i)
	{
		this.preview = i;
	}
	
	public int getPreview()
	{
		return this.preview;
	}
	
	private void setScreen()
	{
		BufferedImage frame = null;
		List<BufferedImage> imgs = new ArrayList<BufferedImage>();

		if (this.file == null || !this.file.exists())
			return;

		long headLen = -1;
		long len = -1;
		MovieBox movie = null;

		try
		{
			movie = MP4Util.parseMovie(this.file);
		}
		catch (IOException e1)
		{
			e1.printStackTrace();
		}
		if (movie == null)
			return;
		len = movie.getDuration();
		if (len <= 0)
			return;
		headLen = movie.getHeader().headerSize();
		FrameGrab grab = null;
		try
		{
				try {
					grab = new FrameGrab(NIOUtils.rwFileChannel(this.file));
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (JCodecException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		}
		catch (Exception z)
		{}
		if (grab == null)
			return;
		for (int count=(int)headLen; count < len; count++)
		{
			try
			{
				try
				{
//					grab.seekToFramePrecise(count);
					this.screen = grab.getFrame(this.file, count);//grab.getFrame();
					return;
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
				catch (JCodecException e)
				{
					e.printStackTrace();
				}
			}
			catch (Exception z)
			{}
		}
		return;
	}
	
	public BufferedImage getScreen()
	{
		return this.screen;
	}
}
