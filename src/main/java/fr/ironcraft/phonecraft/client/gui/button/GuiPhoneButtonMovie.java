package fr.ironcraft.phonecraft.client.gui.button;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;

import org.lwjgl.opengl.GL11;

import fr.ironcraft.phonecraft.client.gui.GuiPhoneInGame;
import fr.ironcraft.phonecraft.utils.ImageLoader;
import fr.ironcraft.phonecraft.utils.camera.MoviePlayer;

public class GuiPhoneButtonMovie extends GuiPhoneButton
{
	private MoviePlayer mp;
	public GuiPhoneButtonMovie(GuiPhoneInGame gui, int id, int x, int y, MoviePlayer mp)
	{
		this(gui, id, x, y, 25, 17, mp, "");
	}

	public GuiPhoneButtonMovie(GuiPhoneInGame gui, int id, int x, int y, MoviePlayer mp, String title)
	{
		this(gui, id, x, y, 25, 17, mp, title);
	}

	public GuiPhoneButtonMovie(GuiPhoneInGame gui, int id, int x, int y, int width, int height, MoviePlayer mp, String title)
	{
		super(gui, id, x, y, width, height, title);
		this.mp = mp;
	}

	public void drawButton(Minecraft p_146112_1_, int p_146112_2_, int p_146112_3_)
	{
		if (this.visible)
		{
			GL11.glPushMatrix();
			GL11.glTranslatef(-0.5F, 0, 0);
			FontRenderer fontrenderer = p_146112_1_.fontRenderer;
			GL11.glColor4f(1.0F, 1.0F, 1.0F, gui.getTransparency());
			this.field_146123_n = p_146112_2_ >= this.xPosition && p_146112_3_ >= this.yPosition && p_146112_2_ < this.xPosition + this.width && p_146112_3_ < this.yPosition + this.height;
			int k = this.getHoverState(this.field_146123_n);

			if (mp.getScreen() == null)
			{
				GL11.glPushMatrix();
				GL11.glColor4f(1, 1, 1, gui.getTransparency());
				GL11.glEnable(GL11.GL_BLEND);
				GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				gui.mc.renderEngine.bindTexture(gui.getTextureIcons_white());
				GL11.glTranslatef(this.xPosition, this.yPosition, 0);
				GL11.glRotatef((System.currentTimeMillis() / 10) % 60 * 6, 0, 0, 1);
				GL11.glTranslatef(-8, -8, 0);
				this.drawTexturedModalRect(0, 0, 66 % 16 * 16, 64, 16, 16);
				GL11.glDisable(GL11.GL_BLEND);
				GL11.glPopMatrix();
			}
			else
			{
				GL11.glColor4f(1, 1, 1, gui.getTransparency());
				if (this.field_146123_n && gui.getFocus())
					gui.drawAbsoluteRect(this.xPosition - 1, this.yPosition - 1, this.xPosition + this.width + 1, this.yPosition + this.height + 2, 0x550000, gui.getTransparency());
				else
					gui.drawAbsoluteRect(this.xPosition - 1, this.yPosition - 1, this.xPosition + this.width + 1, this.yPosition + this.height + 2, 0xb2b2b2, gui.getTransparency());
				GL11.glColor4f(1, 1, 1, gui.getTransparency());
				GL11.glPushMatrix();
				if (mp.getPreview() < 0)
					mp.setPreview(new ImageLoader().setupTexture(mp.getScreen()));
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, mp.getPreview());
				GL11.glEnable(GL11.GL_BLEND);
				GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				GL11.glTranslatef(this.xPosition, this.yPosition, 0);
				GL11.glScalef(0.1F, 0.07F, 1F);
				this.drawTexturedModalRect(0, 0, 0, 0, 256, 256);
				GL11.glDisable(GL11.GL_BLEND);
				GL11.glPopMatrix();
			}
			this.mouseDragged(p_146112_1_, p_146112_2_, p_146112_3_);
			int l = 14737632;

			if (packedFGColour != 0)
				l = packedFGColour;
			else if (!this.enabled)
				l = 10526880;
			else if (this.field_146123_n)
				l = 16777120;
			this.drawCenteredString(fontrenderer, this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, l);
			GL11.glPopMatrix();
		}
	}
}
