package fr.ironcraft.phonecraft.client.gui.button;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import fr.ironcraft.phonecraft.client.gui.GuiPhoneInGame;

public class GuiPhoneButtonMenu extends GuiPhoneButton
{
	private boolean hasBackground;
	
	public GuiPhoneButtonMenu(GuiPhoneInGame gui, int id, int x, int y, String title, boolean hasBackground)
    {
        super(gui, id, x, y, 23, 23, title);
        this.hasBackground = hasBackground;
    }
    
	public GuiPhoneButtonMenu(GuiPhoneInGame gui, int id, int x, int y, int width, int height, String title, boolean hasBackground)
	{
		super(gui, id, x, y, width, height, title);
		this.hasBackground = hasBackground;
	}
	
    public void drawButton(Minecraft par1Minecraft, int x, int y)
    {
        if (this.visible)
        {
        	GL11.glEnable(GL11.GL_BLEND);
        	OpenGlHelper.glBlendFunc(770, 771, 1, 0);
        	GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        	if (hasBackground)
        		gui.drawAbsoluteGradientRect(this.xPosition, this.yPosition, this.xPosition + this.width - 2, this.yPosition + this.height, 0x626262, 0x424242, gui.getTransparency());
        	if (this.field_146123_n && gui.getFocus())
        		gui.drawAbsoluteGradientRect(this.xPosition, this.yPosition, this.xPosition + this.width - 2, this.yPosition + this.height, 0x55d2d2d2, 0x55000000, gui.getTransparency() - 0.6F);
        }
        super.drawButton(par1Minecraft, x, y);
    }
}