package fr.ironcraft.phonecraft.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

/**
 * @authors Dermenslof, DrenBx
 */
public class ModelRelayTelecom extends ModelBase
{
    ModelRenderer PillarH;
    ModelRenderer Pillar;
    ModelRenderer support2H;
    ModelRenderer support2B;
    ModelRenderer antenna2;
    ModelRenderer support3H;
    ModelRenderer support3B;
    ModelRenderer antenn3a;
    ModelRenderer support4H;
    ModelRenderer support4B;
    ModelRenderer antenna4;
    ModelRenderer support1H;
    ModelRenderer support1B;
    ModelRenderer antenna1;
  
  public ModelRelayTelecom()
  {
    textureWidth = 64;
    textureHeight = 32;
    
      PillarH = new ModelRenderer(this, 30, 0);
      PillarH.addBox(-1F, -13F, -1F, 2, 1, 2);
      PillarH.setRotationPoint(0F, 24F, 0F);
      PillarH.setTextureSize(64, 32);
      PillarH.mirror = true;
      setRotation(PillarH, 0F, 0F, 0F);
      Pillar = new ModelRenderer(this, 16, 0);
      Pillar.addBox(-2F, -12F, -2F, 4, 12, 4);
      Pillar.setRotationPoint(0F, 24F, 0F);
      Pillar.setTextureSize(64, 32);
      Pillar.mirror = true;
      setRotation(Pillar, 0F, 0F, 0F);
      support2H = new ModelRenderer(this, 32, 0);
      support2H.addBox(-1F, -9F, 0F, 2, 1, 7);
      support2H.setRotationPoint(0F, 24F, 0F);
      support2H.setTextureSize(64, 32);
      support2H.mirror = true;
      setRotation(support2H, 0.7853982F, 1.570796F, 0F);
      support2B = new ModelRenderer(this, 32, 0);
      support2B.addBox(-1F, -1F, -8F, 2, 1, 7);
      support2B.setRotationPoint(0F, 24F, 0F);
      support2B.setTextureSize(64, 32);
      support2B.mirror = true;
      setRotation(support2B, -0.7853982F, 1.570796F, 0F);
      antenna2 = new ModelRenderer(this, 0, 0);
      antenna2.addBox(-3F, -16F, -7F, 6, 16, 2);
      antenna2.setRotationPoint(0F, 24F, 0F);
      antenna2.setTextureSize(64, 32);
      antenna2.mirror = true;
      setRotation(antenna2, 0F, 1.570796F, 0F);
      support3H = new ModelRenderer(this, 32, 0);
      support3H.addBox(-1F, -9F, 0F, 2, 1, 7);
      support3H.setRotationPoint(0F, 24F, 0F);
      support3H.setTextureSize(64, 32);
      support3H.mirror = true;
      setRotation(support3H, 0.7853982F, 3.141593F, 0F);
      support3B = new ModelRenderer(this, 32, 0);
      support3B.addBox(-1F, -1F, -8F, 2, 1, 7);
      support3B.setRotationPoint(0F, 24F, 0F);
      support3B.setTextureSize(64, 32);
      support3B.mirror = true;
      setRotation(support3B, -0.7853982F, 3.141593F, 0F);
      antenn3a = new ModelRenderer(this, 0, 0);
      antenn3a.addBox(-3F, -16F, -7F, 6, 16, 2);
      antenn3a.setRotationPoint(0F, 24F, 0F);
      antenn3a.setTextureSize(64, 32);
      antenn3a.mirror = true;
      setRotation(antenn3a, 0F, 3.141593F, 0F);
      support4H = new ModelRenderer(this, 32, 0);
      support4H.addBox(-1F, -9F, 0F, 2, 1, 7);
      support4H.setRotationPoint(0F, 24F, 0F);
      support4H.setTextureSize(64, 32);
      support4H.mirror = true;
      setRotation(support4H, 0.7853982F, -1.570796F, 0F);
      support4B = new ModelRenderer(this, 32, 0);
      support4B.addBox(-1F, -1F, -8F, 2, 1, 7);
      support4B.setRotationPoint(0F, 24F, 0F);
      support4B.setTextureSize(64, 32);
      support4B.mirror = true;
      setRotation(support4B, -0.7853982F, -1.570796F, 0F);
      antenna4 = new ModelRenderer(this, 0, 0);
      antenna4.addBox(-3F, -16F, -7F, 6, 16, 2);
      antenna4.setRotationPoint(0F, 24F, 0F);
      antenna4.setTextureSize(64, 32);
      antenna4.mirror = true;
      setRotation(antenna4, 0F, -1.570796F, 0F);
      support1H = new ModelRenderer(this, 32, 0);
      support1H.addBox(-1F, -9F, 0F, 2, 1, 7);
      support1H.setRotationPoint(0F, 24F, 0F);
      support1H.setTextureSize(64, 32);
      support1H.mirror = true;
      setRotation(support1H, 0.7853982F, 0F, 0F);
      support1B = new ModelRenderer(this, 32, 0);
      support1B.addBox(-1F, -1F, -8F, 2, 1, 7);
      support1B.setRotationPoint(0F, 24F, 0F);
      support1B.setTextureSize(64, 32);
      support1B.mirror = true;
      setRotation(support1B, -0.7853982F, 0F, 0F);
      antenna1 = new ModelRenderer(this, 0, 0);
      antenna1.addBox(-3F, -16F, -7F, 6, 16, 2);
      antenna1.setRotationPoint(0F, 24F, 0F);
      antenna1.setTextureSize(64, 32);
      antenna1.mirror = true;
      setRotation(antenna1, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(entity, f, f1, f2, f3, f4, f5);
    PillarH.render(f5);
    Pillar.render(f5);
    support2H.render(f5);
    support2B.render(f5);
    antenna2.render(f5);
    support3H.render(f5);
    support3B.render(f5);
    antenn3a.render(f5);
    support4H.render(f5);
    support4B.render(f5);
    antenna4.render(f5);
    support1H.render(f5);
    support1B.render(f5);
    antenna1.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
  }

}

