package fr.ironcraft.phonecraft.network;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityAntena;
import fr.ironcraft.phonecraft.common.tileentities.TileEntityRelayTelecom;
import fr.ironcraft.phonecraft.packet.PacketQrCode;
import fr.ironcraft.phonecraft.packet.PacketRelayTelecom;

/**
 * @authors Dermenslof, DrenBx
 */
public class ThreadUpdateNetWork implements Runnable
{
	private TileEntity tile;
	private boolean connected;
	private World w;
	private List<TileEntity> queue = new ArrayList<TileEntity>();
	private List<TileEntity> valid = new ArrayList<TileEntity>();

	public ThreadUpdateNetWork(World w, int x, int y, int z, boolean connected)
	{
		this.w= w;
		this.tile = new TileEntity();
		this.tile.xCoord = x;
		this.tile.yCoord = y;
		this.tile.zCoord = z;
		this.connected = connected;
	}
	
	public ThreadUpdateNetWork(TileEntity te, boolean connected)
	{
		this.w = te.getWorldObj();
		this.tile = te;
		this.connected = connected;
	}

	public void run()
	{
		this.update(this.tile, this.connected, false);
		valid.clear();
		for (TileEntity te : queue)
			update(te, true, true);
	}

	private void update(TileEntity ref, boolean connected, boolean init)
	{
		World w = this.w;
		int i = ref.xCoord;
		int j = ref.yCoord;
		int k = ref.zCoord;
		TileEntity te = null;
		for (int x=-200; x < 200; x++)
		{
			for (int y=-200; y < 200; y++)
			{
				for (int z=-200; z < 200; z++)
				{
					te = w.getTileEntity(i + x, j+ y, k + z);
					if (te == null || ref == te || te == this.tile)
						continue;
					if (te instanceof TileEntityRelayTelecom)
					{
						TileEntityRelayTelecom tert = (TileEntityRelayTelecom)te;
						if (tert.isConnected == connected)
							continue;
						tert.isConnected = connected;
						Phonecraft.packetPipeline1.sendToAll(new PacketRelayTelecom(te.xCoord, te.yCoord, te.zCoord, connected));
						if (!valid.contains(te))
						{
							valid.add(te);
							update(te, connected, init);
						}
					}
					else if (!init && te instanceof TileEntityAntena && !queue.contains(te))
							queue.add(te);
				}
			}	
		}
	}
}
