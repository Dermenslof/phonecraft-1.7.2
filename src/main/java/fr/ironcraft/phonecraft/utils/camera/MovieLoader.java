package fr.ironcraft.phonecraft.utils.camera;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jcodec.api.FrameGrab;
import org.jcodec.api.JCodecException;
import org.jcodec.common.NIOUtils;
import org.jcodec.common.SeekableByteChannel;
import org.jcodec.containers.mp4.MP4Util;
import org.jcodec.containers.mp4.boxes.MovieBox;

import fr.ironcraft.phonecraft.client.ClientProxy;
import fr.ironcraft.phonecraft.utils.ImageLoader;

public class MovieLoader implements Runnable
{
	private File file;
	private MoviePlayer player;

	public MovieLoader(File file, MoviePlayer imgLoader)
	{
		this.file = file;
		this.player = imgLoader;
	}

	@Override
	public void run()
	{
		BufferedImage frame = null;
		List<BufferedImage> imgs = new ArrayList<BufferedImage>();

		if (this.file == null || !this.file.exists())
			return;

		long headLen = -1;
		long len = -1;
		MovieBox movie = null;

		try
		{
			movie = MP4Util.parseMovie(this.file);
		}
		catch (IOException e1)
		{
//			e1.printStackTrace();
		}
		if (movie == null)
			return;
		len = movie.getDuration();
		if (len <= 0)
			return;
		headLen = movie.getHeader().headerSize();
		FrameGrab grab = null;
		try
		{
			grab = new FrameGrab(NIOUtils.rwFileChannel(this.file));
		}
		catch (IOException e1)
		{
//			e1.printStackTrace();
		}
		catch (JCodecException e1)
		{
//			e1.printStackTrace();
		}
		if (grab == null)
			return;
		for (int count=(int)headLen; count < len; count++)
		{
			try
			{
				grab.seekToFramePrecise(count);
				frame = grab.getFrame();//getFrame(this.file, count);
				if (frame != null)
					imgs.add(frame);
			}
			catch (IOException e)
			{
//				e.printStackTrace();
			}
			catch (JCodecException e)
			{
//				e.printStackTrace();
			}
		}
		this.player.setFrames(imgs);
		this.player.setLoaded();
	}
}