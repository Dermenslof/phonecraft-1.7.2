package fr.ironcraft.phonecraft.client.gui;

import java.awt.List;
import java.io.File;
import java.lang.reflect.Array;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.stats.AchievementList;
import net.minecraft.world.WorldSettings;
import net.minecraftforge.common.config.Configuration;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.api.PhoneApps;
import fr.ironcraft.phonecraft.client.AppRegistry;
import fr.ironcraft.phonecraft.client.ClientProxy;
import fr.ironcraft.phonecraft.client.PhoneAchievements;
import fr.ironcraft.phonecraft.client.gui.button.EnumPhoneButton;
import fr.ironcraft.phonecraft.client.gui.button.GuiPhoneButton;
import fr.ironcraft.phonecraft.client.gui.button.GuiPhoneButtonMenu;
import fr.ironcraft.phonecraft.loader.AppServiceLoader;
import fr.ironcraft.phonecraft.utils.ImageLoader;
import fr.ironcraft.phonecraft.utils.TextUtils;
import fr.ironcraft.phonecraft.utils.TextureUtils;

/**
 * @author Dermenslof, DrenBx
 */
@SideOnly(Side.CLIENT)
public class GuiPhoneMenu extends GuiPhoneInGame
{
	private Object init;
	public static boolean isMap;

	public GuiPhoneMenu(Minecraft par1Minecraft)
	{
		super(par1Minecraft);
		this.transparency = 0F;
		this.isHome = true;
		if(this.init == null)
			this.init = this;
		this.mc.thePlayer.triggerAchievement(PhoneAchievements.openPhone);
	}

	public GuiPhoneMenu(Minecraft par1Minecraft, boolean home)
	{
		super(par1Minecraft);
		this.transparency = 1F;
		this.isHome = home;
	}

	@Override
	public void updateScreen()
	{
		super.updateScreen();
		boolean hideButtons = isHome && !isAnimated;
		for (Object b: this.buttonList)
		{
			if (((GuiButton)b).id >= 2 && ((GuiPhoneButton)b).id <= 5)
				((GuiButton)b).visible = hideButtons;
			if (((GuiButton)b).id == 6)
				((GuiButton)b).visible = !hideButtons;
		}
	}

	public void initGui()
	{
		this.buttonList.clear();
		super.initGui();

		//buttons;
		int[] ic = {16, 17, 19, 84};
		for (int t=0; t < 4; ++t)
		{
			GuiPhoneButton b = new GuiPhoneButtonMenu(this, t + 2, 1 + (t * 23), this.getScreenHeight() - 19, 23, 18, "", true);
			b.setIcon(this.getTextureIcons(), 3, 1, ic[t], 1F);
			this.buttonList.add(b);
		}
		GuiPhoneButton b = new GuiPhoneButtonMenu(this, 6, getScreenWidth() - 16, 4, 12, 10, "", false);
		b.setIcon(this.getTextureIcons(), 0, 0, 66, 0.6F);
		this.buttonList.add(b);

		//wallpaper
		int preWallpaper = Phonecraft.config.get(Configuration.CATEGORY_GENERAL, "wallpaper", -1).getInt();
		if (preWallpaper != this.wallpaper)
		{
			this.wallpaper = preWallpaper > -1 ? ClientProxy.imageLoader.setupWallpapers("wallpapers:" + new File(Phonecraft.phoneFolder, "wallpapers").listFiles()[preWallpaper].getName()) : -1;
			wallpaperName = preWallpaper > -1 ? new File(Phonecraft.phoneFolder, "wallpapers").listFiles()[preWallpaper].getName() : "default";
		}
		this.screen = 0;
		this.clickX = this.clickY = this.releaseX = this.releaseY = -1;
	}

	@Override
	protected void mouseMovedOrUp(int x, int y, int which)
	{
		super.mouseMovedOrUp(x, y, which);
		if (this.taskbarshift > 0)
			return;
		if (which != 0)
			return;
		if (getFocus())
		{
			switch (this.buttonType)
			{
			case APP:
				if(this.app > 0)
				{
					System.out.println("APP ID : " + app);
					try
					{
						GuiPhoneInGame appGui = AppRegistry.getAppGuiById(app - 1);
						if (app > 3)
							appGui = appGui.getClass().newInstance();
						if(appGui != null)
							this.mc.displayGuiScreen(appGui);
					}
					catch(NullPointerException e){}
					catch (InstantiationException e){}
					catch (IllegalAccessException e){}
				}
				break;
			default:
				;
			}
		}
	}

	protected void actionPerformed(GuiButton button)
	{
		super.actionPerformed(button);
		switch (button.id)
		{
		case 2:
			this.screen = 1;
			this.isCamera = true;
			this.mc.displayGuiScreen(new GuiPhoneCamera(this.mc));
			break;
		case 3:
			this.screen = 2;
			this.mc.displayGuiScreen(new GuiPhoneMessages(this.mc));
			break;
		case 4:
			this.screen = 3;
			this.mc.displayGuiScreen(new GuiPhoneContacts(this.mc));
			break;
		case 5:
			this.isHome = false;
			this.screen = 0;
			break;
		case 6:
			try
			{
				File phoneappdir = new File(this.mc.mcDataDir, Phonecraft.phoneFolder + "apps");
				int len = AppRegistry.getAppsList().size();
				AppRegistry.instance().clearList();
				AppRegistry.instance().init();
				AppRegistry.instance().registerSystemApps();
				AppRegistry.instance().registerUserApps();
				if (len != AppRegistry.getAppsList().size())
					this.mc.refreshResources();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			break;
		}
	}

	public void drawScreen(int par1, int par2, float par3)
	{
		super.drawScreen(par1, par2, par3);
		this.drawMainMenu(par1, par2, par3);
		this.onMouseOverPhone(par1, par2);
		
		//buttons background
		if (this.isHome)
		{
			GL11.glPushMatrix();
			GL11.glColor4f(1, 1, 1, this.transparency);
			this.drawGradientRect(0, this.getScreenHeight() - 20, this.getScreenWidth(), this.getScreenHeight(), 0x323232, 0x111111, 0F);
			GL11.glPopMatrix();
		}
		for (Object b: this.buttonList)
			((GuiButton)b).drawButton(mc, par1, par2);
	}

	private void drawHome(int par1, int par2, float par3)
	{
		//Horloge mainMenu
		Date d = new Date();

		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glColor4f(1,  1,  1,  this.transparency);
		this.mc.renderEngine.bindTexture(this.getTexturePhone());
		this.drawImageOnScreen(5, 15, 113, 78, 42, 42);
		GL11.glTranslatef(this.getScreenPosX() + 26, this.getScreenPosY() + 36, 0);
		GL11.glRotatef(d.getMinutes() * 6 + 180,  0,  0,  1);
		this.drawTexturedModalRect(-1, 0, 156, 79, 2, 15);
		GL11.glPopMatrix();

		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glColor4f(1,  0,  0,  this.transparency);

		GL11.glTranslatef(this.getScreenPosX() + 26, this.getScreenPosY() + 36, 0);
		GL11.glRotatef(d.getSeconds() * 6 + 180,  0, 0, 1);
		this.drawTexturedModalRect(-1, 0, 156, 79, 2, 15);

		GL11.glPopMatrix();

		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glColor4f(1,  1,  1,  this.transparency);

		GL11.glTranslatef(this.getScreenPosX() + 26, this.getScreenPosY() + 36, 0);
		GL11.glRotatef(d.getHours() * 30 + 180,  0,  0,  1);
		this.drawTexturedModalRect(-1, 0, 156, 79, 2, 10);
		GL11.glPopMatrix();

		//------------------------------------------

		GL11.glPushMatrix();
		Calendar cal = Calendar.getInstance();
		String Day = TextUtils.getLanguage("day." + cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US).toLowerCase());
		if (Day.charAt(0) >= 'a')
			Day = "" + (char)(Day.charAt(0) - 32) + Day.substring(1);

		//		GL11.glTranslatef(this.width - 30 + this.shift, this.height - 170, 0);
		GL11.glScalef(0.5F,  0.5F,  1);
		this.drawString(Day, (int)((this.getScreenPosX() + 59) / 0.5F), (int)((this.getScreenPosY() + 35) / 0.5F), 0xffffcc66, 0F);
		GL11.glPopMatrix();

		GL11.glPushMatrix();
		GL11.glTranslatef(this.getScreenPosX() + this.getScreenWidth() - 15, this.getScreenPosY() + 40, 0);
		GL11.glScalef(0.5F,  0.5F,  1);
		this.drawString(cal.get(Calendar.DAY_OF_MONTH)
				+ "/" + (cal.get(Calendar.MONTH) + 1)
				+ "/" + cal.get(Calendar.YEAR), - (int)((this.getFont().getStringWidth(cal.get(Calendar.DAY_OF_MONTH)
						+ "/" + (cal.get(Calendar.MONTH) + 1)
						+ "/" + cal.get(Calendar.YEAR)) / 2) / 0.7F + 2), 0, 0xffd2d2d2, 0F);
		GL11.glPopMatrix();
	}

	private void drawMainMenu(int par1, int par2, float par3)
	{
		if(this.screen == -1)
			return;
		if(this.isHome)
		{
			drawHome(par1, par2, par3);
			return;
		}
		//pagination
		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glColor4f(1,  1,  1,  this.transparency);
		GL11.glTranslatef(-0.5F, 0, 0);
		this.mc.renderEngine.bindTexture(this.getTexturePhone());
		for(int t=0; t<3; t++)
			this.drawImageOnScreen(35 + t * 10, 17, 44, 227, 3, 3);
		//page actuelle
		GL11.glTranslatef(-0.25F, -0.5F, 0);
		this.drawImageOnScreen(34 + (this.page * 10), 12, 0, 230, 15, 15);
		GL11.glPopMatrix();
		drawPage();
	}

	private void drawPage()
	{
		int t = (this.page * 16);
		for(int line = 0; line < 4; line++)
		{
			for(int col = 0; col < 4; col++)
			{
				t++;
				try
				{
					String title = null;
					PhoneApps app = AppRegistry.getAppById(t - 1);
					if(app == null)
						return;
					title = app.appname();

					if(title == null)
						return;
					boolean focus = this.buttonType == buttonType.APP && this.app == t;
					float zoom = focus ? 1.2F : 1F;
					float trX = focus ? -2 : 0;
					float trY = focus ? -2 : 0;
					GL11.glPushMatrix();
					GL11.glEnable(GL11.GL_BLEND);
					GL11.glColor4f(1,  1,  1, this.transparency);
					if (app.appname().equals("Images"))
					{
						GL11.glTranslated((this.getScreenPosX() + 3 + (col * 25)) + trX, (this.getScreenPosY() + 30 + line * 30) + trY, 0);
						GL11.glScalef(1F * zoom, 1F * zoom, 1);
						this.mc.renderEngine.bindTexture(this.getTextureIcons_white());
						this.drawTexturedModalRect(0, 0, 16, 80, 16, 16);
					}
					else if (app.appname().equals("Settings"))
					{
						GL11.glTranslated((this.getScreenPosX() + 3 + (col * 24)) + trX, (this.getScreenPosY() + 30 + line * 30) + trY, 0);
						GL11.glScalef(1F * zoom, 1F * zoom, 1);
						this.mc.renderEngine.bindTexture(this.getTextureIcons_white());
						this.drawTexturedModalRect(0, 0, 62, 64, 16, 16);
					}
					else
					{
						GL11.glBindTexture(GL11.GL_TEXTURE_2D, app.getIcon());
						GL11.glTranslated((this.getScreenPosX() + 5 + (col * 23)) + trX, (this.getScreenPosY() + 30 + line * 30) + trY, 0);
						GL11.glScalef((1F / 16F) * zoom, (1F / 16F) * zoom, 1);
						this.drawTexturedModalRect(0, 0, 0, 0, 256, 256);
					}
					GL11.glPopMatrix();
				}
				catch(Exception e)
				{
					e.printStackTrace();
					return;
				}
			}
		}
	}

	private void onOverApps()
	{
		if(!this.getFocus() || this.isHome || this.taskbarshift > 0)
			return;
		if(this.mouseX >= this.getScreenPosX() && this.mouseX <= this.getScreenPosX() + this.getScreenWidth())
		{
			if(this.mouseY >= this.getScreenPosY() && this.mouseY <= this.getScreenPosX() + this.getScreenHeight())
			{
				int t = (this.page * 16);
				for(int line = 0; line < 4; line++)
				{
					for(int col = 0; col < 4; col++)
					{
						t++;
						try
						{
							String title = null;
							PhoneApps app = AppRegistry.getAppById(t - 1);
							if(app == null)
								return;
							title = app.appname();
							if(title == null)
								return;
							if(this.mouseX >= 3 + this.getScreenPosX() + (col * 23) && this.mouseX <= 3 + this.getScreenPosX() + (col * 23) + 20)
							{
								if(this.mouseY >= this.getScreenPosY() + 30 + (line * 30) && this.mouseY <= this.getScreenPosY() + 30 + (line * 30) + 20)
								{
									this.buttonType = EnumPhoneButton.APP;
									this.app = t;
								}
							}
						}
						catch(Exception e)
						{
							e.printStackTrace();
							return;
						}
					}
				}
			}
		}
	}

	protected void onMouseOverPhone(int x, int y)
	{
		super.onMouseOverPhone(x, y);
		if(this.isTactile && !this.mouseIsDrag && this.clickX >= 0)
		{
			if(-this.clickX+this.releaseX >40)
				this.page--;
			else if(-this.clickX+this.releaseX <-40)
				this.page++;
			this.isTactile = false;
			this.clickX = this.releaseX = this.releaseY = this.clickY = -1;
		}
		if(this.page < 0)
			this.page = 2;
		if(this.page > 2)
			this.page = 0;
		onOverApps();
	}
}