package fr.ironcraft.phonecraft.client;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import apps.exemple.arca.GuiPhoneArcaMenu;

import com.google.common.collect.ImmutableList;

import fr.ironcraft.phonecraft.Phonecraft;
import fr.ironcraft.phonecraft.api.PhoneApps;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneImages;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneInGame;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneSettings;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneVideo;
import fr.ironcraft.phonecraft.loader.AppServiceLoader;

/**
 * @author Dermenslof, DrenBx
 */
public class AppRegistry
{
	private static AppRegistry instance;
	private static List<PhoneApps> appsList = new ArrayList<PhoneApps>();
	private Minecraft mc;
	public AppServiceLoader appFinder;
	
	protected AppRegistry(Minecraft minecraft)
	{
		this.mc = minecraft;
		instance = this;
		
		init();
		registerSystemApps();
		registerUserApps();
		System.out.println("[PhoneCraft] " + (appsList.size() - 2) + " apps found");
	}
	
	
	public void registerUserApps()
	{
		if(appFinder.getAppCollection() != null)
		{
			for(PhoneApps app : appFinder.getAppCollection())
			{
				boolean exist = false;
				for (PhoneApps a : this.appsList)
				{
					if (a.appname().equals(app.appname()))
					{
						exist = true;
						break;
					}
				}
				if (!exist)
				{
					this.appsList.add(app);
					System.out.println("[PhoneApps] " + app.appname() + " "+ app.version() + " ---> DONE");
				}
			}
		}
		else
			new Exception("[PhoneCraft]: AppServiceLoader have encontred an exception");
	}

	public void registerSystemApps()
	{
		this.appsList.add(new SystemApp("Settings", "1.0.0", new GuiPhoneSettings(mc)));
		this.appsList.add(new SystemApp("Images", "1.0.0", new GuiPhoneImages(mc)));
		this.appsList.add(new SystemApp("Videos", "1.0.0", new GuiPhoneVideo(mc)));
	}

	public void init()
	{
		try
		{
			File phoneappdir = new File(this.mc.mcDataDir, Phonecraft.phoneFolder + "apps");
			appFinder = new AppServiceLoader();
			appFinder.search(phoneappdir);
		}
		catch (Exception e)
		{
			System.out.println("[PhoneCraft] No app found");
			e.printStackTrace();
		}
	}
	
	public static ImmutableList<PhoneApps> getAppsList()
	{
		return instance().appsList != null ? ImmutableList.copyOf(instance().appsList) : ImmutableList.<PhoneApps>of();
	}


	public static AppRegistry instance()
	{
	        return instance;
	}

	public static PhoneApps getAppById(int id)
	{
		if(id < appsList.size())
			return appsList.get(id);
		else
			return null;
	}

	public static void clearList()
	{
		appsList.clear();
	}
	
	public static GuiPhoneInGame getAppGuiById(int app)
	{
		return getAppById(app).ScreenInstance();
	}
}
