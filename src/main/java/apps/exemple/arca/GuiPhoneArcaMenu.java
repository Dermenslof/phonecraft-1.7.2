package apps.exemple.arca;

import java.awt.image.BufferedImage;
import java.util.Random;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.ironcraft.phonecraft.client.ClientProxy;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneInGame;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneMenu;
import fr.ironcraft.phonecraft.client.gui.GuiPhoneNoNetWork;
import fr.ironcraft.phonecraft.client.gui.button.EnumPhoneButton;
import fr.ironcraft.phonecraft.utils.ImageLoader;
import fr.ironcraft.phonecraft.utils.TextUtils;

enum State
{
	MENU, GAME, SCORE
}

@SideOnly(Side.CLIENT)
public class GuiPhoneArcaMenu extends GuiPhoneInGame
{
	public static Minecraft mc = Minecraft.getMinecraft();
	private float zoomTitle;
	private static State state = State.MENU;
	public int lastButton;
	private boolean sens;
	private int bar;
	private float ballX;
	private float ballY;
	private Vector2f ballMove;
	private static Boolean[][] bricks;

	public GuiPhoneArcaMenu()
	{
		super(mc);
	}

	public void initGui()
	{
		super.initGui();
		state = State.MENU;
		zoomTitle = 0.8F;
		sens = true;
		lastButton = -1;
		
		bar = getScreenWidth() / 2 - 5;
	}

	public void updateScreen()
	{
		super.updateScreen();
		this.zoomTitle += this.sens ? 0.1F : -0.1F;
		if (this.zoomTitle <= 0.8F || this.zoomTitle >= 2)
			this.sens = !this.sens;
		if (this.isInScreen() && state == State.GAME)
		{
			if (ballY + ballMove.y >= getScreenHeight() - 1)
			{
				ballY = getScreenHeight() - 1;
				return;
			}
			if (ballX > getScreenWidth() - 2 || ballX < 1)
				ballMove.setTheta(180.0D - ballMove.getTheta());
			if (ballY < 0 || ballY > getScreenHeight() - 6)
			{
				if (ballY > getScreenHeight() - 6)
				{
					if (ballX >= bar && ballX <= bar + 20 - 1)
					{
						int offset = (int)ballX - bar;
						int percent = (offset * 100) / 20;
						if (percent < 40)
							ballMove.setTheta(-ballMove.getTheta() - ((100 - percent * 2)  * 45) / 100);
						else if (percent > 60)
							ballMove.setTheta(-ballMove.getTheta() + (((percent - 50)* 2)  * 45) / 100);
						else
							ballMove.setTheta(-ballMove.getTheta());
					}
				}
				else
					ballMove.setTheta(-ballMove.getTheta());
			}
			int size = getScreenWidth() / 15;
			for (int y=0; y < 15; ++y)
			{
				for (int x=0; x < 15; ++x)
				{
					if (ballX + ballMove.x >= 1 + x * size && ballX + ballMove.x <= 1 + size + x * size)
					{
						if (ballY + ballMove.y >= 10 + y * 4 && ballY + ballMove.y <= 14 + y * 4)
						{
							if (bricks[y][x] == null)
							{
								if (ballY < 10 + y * 4 || ballY > 14 + y * 4)
									ballMove.setTheta(-ballMove.getTheta());
								else if (ballX < 1 + x * size || ballX > 1 + size + x * size)
									ballMove.setTheta(180.0D - ballMove.getTheta());
								bricks[y][x] = true;
							}
						}
					}
				}
			}
			ballX += ballMove.x;
			ballY += ballMove.y;
		}
	}
	
	@Override
	protected void keyTyped(char par1, int par2)
	{
		super.keyTyped(par1, par2);
	}

	public void mouseClicked(int i, int j, int k)
	{
		if (this.getFocus())
		{
			switch (this.buttonType)
			{
			case BUTTON:
				switch (this.bouton)
				{
				case 10:
					bricks = new Boolean[15][15];
					ballX = getScreenWidth() / 2;
					ballY = getScreenHeight() / 4 * 3;
					ballMove = new Vector2f(2, 2);
					ballMove.setTheta(90);
					state = State.GAME;
					break;
				default:
					;
				}
				break;
			case RETURN:
				if (state == State.GAME && k == 0)
				{
					state = State.MENU;
					return;
				}
			default:
				;
			}
		}
		super.mouseClicked(i, j, k);
	}

	public void handleMouseInput()
	{
		super.handleMouseInput();
	}

	public void drawScreen(int par1, int par2, float par3)
	{
		super.drawScreen(par1, par2, par3);
		drawBackground();
		drawButtons();
		onMouseOverPhone(par1, par2);
	}

	private void drawBackground()
	{
		//black screen
		this.drawRect(0, 0, getScreenWidth(), getScreenHeight(), 0, 1.0F);
		String str;
		if (state == State.MENU)
		{
			str = "Arca";
			//title
			GL11.glPushMatrix();
			GL11.glTranslatef(getScreenPosX() + getScreenWidth() / 2 - (this.fontRendererObj.getStringWidth(str)), getScreenPosY() + 20, 0);
			GL11.glScaled(2, 2, 1);
			((GuiScreen)this).drawString(this.fontRendererObj, str, 0, 0, 0xffffff);
			GL11.glPopMatrix();
			this.drawRect(20, 40, getScreenWidth() - 20, 42, 0xffffff, 0F);
			this.drawRect(22, 42, getScreenWidth() - 18, 44, 0x353535, 0F);

			String[] tab = {I18n.format("gui.arca.play", new Object[0]), I18n.format("gui.arca.highscores", new Object[0])};
			float zoom;
			for (int i=0; i < tab.length; ++i)
			{
				zoom = this.bouton == i + 10 ? zoomTitle / 2F : 0.5F;
				GL11.glPushMatrix();
				GL11.glTranslatef(getScreenPosX() + getScreenWidth() / 2 - (this.fontRendererObj.getStringWidth(tab[i]) / 2) * (0.2F + zoom - 0.1F), getScreenPosY() + 60 + i * 20, 0);
				GL11.glScaled(0.2F + zoom - 0.1F, 0.2F + zoom - 0.1F, 1);
				((GuiScreen)this).drawString(this.fontRendererObj, tab[i], 0, 0, 0xffffff);
				GL11.glPopMatrix();
			}
		}
		else
		{
			int size = getScreenWidth() / 15;
			for (int y=0; y < 15; ++y)
			{
				for (int x=0; x < 15; ++x)
				{
					if (bricks[y][x] == null)
					{
						this.drawHorizontalLine(getScreenPosX() + 1 + x * size, getScreenPosX() + 0 + size + x * size, getScreenPosY() + 10 + y * 4, 0xff757575);
						this.drawVerticalLine(getScreenPosX() + 1 + x * size, getScreenPosY() + 10 + y * 4, getScreenPosY() + 13 + y * 4, 0xff757575);
						
						this.drawHorizontalLine(getScreenPosX() + 1 + x * size, getScreenPosX() + 0 + size + x * size, getScreenPosY() + 13 + y * 4, 0xff252525);
						this.drawVerticalLine(getScreenPosX() + 0 + size + x * size, getScreenPosY() + 10 + y * 4, getScreenPosY() + 13 + y * 4, 0xff252525);
						this.drawRect(2 + x * size, 11 + y * 4, 0 + size + x * size, 13 + y * 4, 0xffffff, 0);
					}
				}
			}
			this.drawRect((int)ballX, (int)ballY, (int)ballX + 1, (int)ballY + 1, 0xffffff, 0);
			this.drawRect(bar, getScreenHeight() - 4, bar + 20, getScreenHeight() - 3, 0xffffff, 0);
			if (!isInScreen())
			{
				String pause = "Pause";
				GL11.glPushMatrix();
				GL11.glTranslatef(getScreenPosX() + getScreenWidth() / 2 - (this.fontRendererObj.getStringWidth(pause)), getScreenPosY() + 100, 0);
				GL11.glScaled(2, 2, 1);
				((GuiScreen)this).drawString(this.fontRendererObj, pause, 0, 0, 0xffffff);
				GL11.glPopMatrix();
			}
			
		}
	}

	private void drawButtons()
	{
		;
	}

	protected void onMouseOverPhone(int x, int y)
	{
		super.onMouseOverPhone(x, y);
		int endPosX = this.getScreenPosX() + this.getScreenWidth();
		if(this.isInScreen())
		{
			if (state != State.MENU)
			{
				bar = x - getScreenPosX() - 10;
				if (bar < 0)
					bar = 0;
				else if (bar > getScreenWidth() - 20)
					bar = getScreenWidth() - 20;
			}
			else
			{
				for(int t=0; t<2; t++)
				{
					if(y >= getScreenPosY() + 55 + t * 20 && y <= getScreenPosY() + 75 + t * 20)
					{

						GL11.glPushMatrix();
						GL11.glPopMatrix();
						this.buttonType = EnumPhoneButton.BUTTON;
						if (this.lastButton != t + 10)
						{
							sens = true;
							zoomTitle = 1F;
						}
						this.bouton = t + 10;
						this.lastButton = t + 10;
					}
				}
			}
		}
	}
}