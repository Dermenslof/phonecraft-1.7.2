package fr.ironcraft.phonecraft;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import fr.ironcraft.phonecraft.client.ClientProxy;
import fr.ironcraft.phonecraft.common.CommonProxy;
import fr.ironcraft.phonecraft.common.blocks.ICBlocks;
import fr.ironcraft.phonecraft.packet.PacketPipelineQrCode;
import fr.ironcraft.phonecraft.packet.PacketPipelineTelecom;

/**
 * @author Dermenslof, DrenBx
 */
@Mod(modid = Phonecraft.MODID, version = Phonecraft.VERSION)
public class Phonecraft
{
	public static String urlFiles;
	public static String phoneFolder = "./PhoneCraft/";
	public static final PacketPipelineQrCode packetPipeline = new PacketPipelineQrCode();
	public static final PacketPipelineTelecom packetPipeline1 = new PacketPipelineTelecom();
	public static Map<String, String> appResourcesPath = new HashMap<String, String>();
	
    public static final String MODID = "phonecraft";
    public static final String VERSION = "0.1";
    
    @Instance(MODID)
    public static Phonecraft instance;
    
    @SidedProxy(clientSide = "fr.ironcraft."+MODID+".client.ClientProxy", serverSide = "fr.ironcraft."+MODID+".common.CommonProxy")
    public static CommonProxy proxy;
    public static Configuration config;
    public static boolean firstOpen;
    
    private static ICBlocks blocks;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
    	File dir = new File(phoneFolder, "wallpapers");
    	if(!dir.exists())
		{
			System.out.println("[PhoneCraft] create main folders");
			dir.mkdirs();
		}
    	config = new Configuration(event.getSuggestedConfigurationFile());
		config.load();
		urlFiles = config.get(Configuration.CATEGORY_GENERAL, "urlFiles", "http://ironcraft.local/").getString(); /* dev value */
		firstOpen = config.get(Configuration.CATEGORY_GENERAL, "firstOpen", true).getBoolean(true); /* dev value */
		if (config.hasChanged())
			config.save();
    	this.blocks = new ICBlocks();
    	this.blocks.init();
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    	proxy.init();
    	packetPipeline.initialise();
    	packetPipeline1.initialise();
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
    	packetPipeline.postInitialise();
    	packetPipeline1.postInitialise();
    	System.out.println("[PhoneCraft] Version: "+VERSION+" was loaded");
    }
    
    public ICBlocks getBlocks()
    {
    	return this.blocks;
    }
}
